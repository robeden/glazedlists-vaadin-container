package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.DisposableMap;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;

import java.util.*;
import java.util.function.Function;


/**
 *
 */
class MappingIDManager<E> extends AbstractIDManager<E> implements ListEventListener<E> {
	private final Function<E,Object> id_function;
	private final boolean cache_keys;

	private DisposableMap<Object,E> key_map;

	private Object first_id = null;
	private Object last_id = null;

	private volatile int size;


	MappingIDManager( EventList<E> source_list, Function<E,Object> id_function,
		boolean cache_keys ) {

		super();

		this.cache_keys = cache_keys;
		this.id_function = Objects.requireNonNull( id_function );

		setSource( source_list );
	}



	@Override
	public int size() {
		return size;
	}



	@Override
	public void setSource( EventList<E> source_list ) {
		if ( this.source_list != null ) {
			this.source_list.removeListEventListener( this );
		}
		if ( this.key_map != null ) {
			this.key_map.dispose();
			this.key_map = null;
		}

		super.setSource( source_list );

		source_list.getReadWriteLock().readLock().lock();
		try {
			if ( cache_keys ) {
				this.key_map =
					GlazedLists.syncEventListToMap( source_list, id_function::apply );
			}

			size = source_list.size();
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}

		source_list.addListEventListener( this );
		rebuildFirstAndLastIds();
	}



	@Override
	public void dispose() {
		if ( key_map != null ) key_map.dispose();
		source_list.removeListEventListener( this );
	}



	@Override
	public E getSourceObject( Object item_id ) {
		final EventList<E> source_list = this.source_list;
		source_list.getReadWriteLock().readLock().lock();
		try {
			if ( cache_keys ) {
				return key_map.get( item_id );
			}
			else {
				int index = indexOfId( item_id );
				if ( index < 0 || index >= source_list.size() ) return null;

				return source_list.get( index );
			}
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}
	}



	@Override
	public Object getIdByIndex( int index ) {
		final EventList<E> source_list = this.source_list;

		E object;
		source_list.getReadWriteLock().readLock().lock();
		try {
			if ( index < 0 || index >= source_list.size() ) return null;

			object = source_list.get( index );
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}

		return id_function.apply( object );
	}

	@Override
	public Object getIdForOldValue( int old_index, E old_value ) {
		return id_function.apply( old_value );
	}



	@Override
	public boolean containsId( Object item_id ) {
		final EventList<E> source_list = this.source_list;
		source_list.getReadWriteLock().readLock().lock();
		try {
			if ( cache_keys ) {
				return key_map.containsKey( item_id );
			}
			else {
				int index = indexOfId( item_id );
				return index >= 0;
			}
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}
	}



	@Override
	public int indexOfId( Object item_id ) {
		Objects.requireNonNull( item_id );

		if ( key_map != null ) return indexOfIdFromMap( item_id );
		else return indexOfIdNoMap( item_id );
	}


	private int indexOfIdFromMap( Object itemId ) {
		E value = key_map.get( itemId );
		if ( value == null ) return -1;

		source_list.getReadWriteLock().readLock().lock();
		try {
			if ( source_supports_random_access ) {
				final int size = source_list.size();
				for ( int i = 0; i < size; i++ ) {
					if ( value == source_list.get( i ) ) {
						return i;
					}
				}
			}
			else {
				Iterator<E> it = source_list.iterator();
				for( int i = 0; it.hasNext(); i++ ) {
					if ( value == it.next() ) {
						return i;
					}
				}
			}
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}

		return -1;
	}

	private int indexOfIdNoMap( Object itemId ) {
		source_list.getReadWriteLock().readLock().lock();
		try {
			if ( source_supports_random_access ) {
				final int size = source_list.size();
				for ( int i = 0; i < size; i++ ) {
					Object id = id_function.apply( source_list.get( i ) );
					if ( itemId.equals( id ) ) return i;
				}
			}
			else {
				Iterator<E> it = source_list.iterator();
				for( int i = 0; it.hasNext(); i++ ) {
					Object id = id_function.apply( it.next() );
					if ( itemId.equals( id ) ) return i;
				}
			}
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}

		return -1;
	}



	@Override
	public boolean isFirstId( Object item_id ) {
		return item_id.equals( first_id );
	}

	@Override
	public Object firstItemId() {
		return first_id;
	}



	@Override
	public boolean isLastId( Object item_id ) {
		return item_id.equals( last_id );
	}

	@Override
	public Object lastItemId() {
		return last_id;
	}



	@Override
	public List<?> getItemIds( int start_index, int number_of_items ) {
		List<Object> to_return = new ArrayList<>( number_of_items );

		source_list.getReadWriteLock().readLock().lock();
		try {
			ListIterator<E> it = source_list.listIterator( start_index );
			for( int i = 0; i < number_of_items && it.hasNext(); i++ ) {
				to_return.add( id_function.apply( it.next() ) );
			}
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}

		return to_return;
	}



	@Override
	public void listChanged( ListEvent<E> listChanges ) {
		size = source_list.size();

		rebuildFirstAndLastIds();
	}

	private void rebuildFirstAndLastIds() {
		source_list.getReadWriteLock().readLock().lock();
		try {
			if ( source_list.isEmpty() ) {
				first_id = null;
				last_id = null;
			}
			else {
				first_id = getIdByIndex( 0 );
				last_id = getIdByIndex( source_list.size() - 1 );
			}
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}
	}
}
