package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.EventList;

import java.util.RandomAccess;


/**
 *
 */
abstract class AbstractIDManager<E> implements IDManager<E> {
	protected volatile EventList<E> source_list;
	protected volatile boolean source_supports_random_access;

	protected AbstractIDManager() {}

	AbstractIDManager( EventList<E> source_list ) {
		setSource( source_list );
	}

	@Override
	public void setSource( EventList<E> source_list ) {
		this.source_list = source_list;
		this.source_supports_random_access = source_list instanceof RandomAccess;
	}

	@Override
	public int size() {
		final EventList<E> source_list = this.source_list;
		source_list.getReadWriteLock().readLock().lock();
		try {
			return source_list.size();
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}
	}
}
