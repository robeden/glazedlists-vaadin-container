package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.TextFilterator;
import ca.odell.glazedlists.matchers.TextMatcherEditor;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.event.FieldEvents;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextField;


/**
 * This listener registers for changes to a {@link com.vaadin.ui.Field} and updates
 * the filter on an {@link com.starlight.vaadin.glazedlists.EventListContainer} when the
 * contents change.
 */
public class StringFieldFilterListener<E> extends TextMatcherEditor<E> {
	private final Field<String> filter_field;

	private final Container.Filterable container;
	private Container.Filter active_filter = null;

	private final InnerListener inner_listener = new InnerListener();



	public StringFieldFilterListener( Field<String> filter_field,
		TextFilterator<? super E> text_filterator, EventListContainer<E> container ) {

		super( text_filterator );

		this.filter_field = filter_field;

		// NOTE: I'm specifically taking EventListContainer to ensure the implementation
		//       (since I need internal classes) and then casting to Filterable to gain
		//       the ability to set filters.
		this.container = ( Container.Filterable ) container;

		if ( filter_field instanceof TextField ) {
			( ( TextField ) filter_field ).addTextChangeListener( inner_listener );
		}

		filter_field.addValueChangeListener( inner_listener );
	}



	/**
	 * Remove all listeners from the filter field and free any other resources.
	 */
	public void dispose() {
		if ( filter_field instanceof TextField ) {
			( ( TextField ) filter_field ).removeTextChangeListener( inner_listener );
		}
		else {
			filter_field.removeValueChangeListener( inner_listener );
		}
	}



	/**
	 * Update the filter text from the contents of the Document.
	 */
	private void refilter( String text ) {
		final int mode = getMode();
		final String[] filters;

		// in CONTAINS mode we treat the string as whitespace delimited
		if ( mode == CONTAINS ) {
			filters = text.split( "[ \t]" );
		}

		// in STARTS_WITH, REGULAR_EXPRESSION, or EXACT modes we use the string in
		// its entirety
		else if ( mode == STARTS_WITH || mode == REGULAR_EXPRESSION || mode == EXACT ) {
			filters = new String[]{ text };
		}

		else {
			throw new IllegalStateException( "Unknown mode: " + mode );
		}

		setFilterText( filters );

		// Remove the old
		if ( active_filter != null ) {
			container.removeContainerFilter( active_filter );
		}
		// Set the new
		active_filter = new MatcherFilter<>( getMatcher() );
		container.addContainerFilter( active_filter );
	}



	private class InnerListener
		implements Property.ValueChangeListener, FieldEvents.TextChangeListener {

		@Override
		public void textChange( FieldEvents.TextChangeEvent event ) {
			refilter( event.getText() );
		}



		@Override
		public void valueChange( Property.ValueChangeEvent event ) {
			refilter( ( String ) event.getProperty().getValue() );
		}
	}
}
