package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.EventList;

import java.lang.reflect.Method;
import java.util.function.Consumer;


/**
 * Decorator that extends functionality of
 * {@link com.starlight.vaadin.glazedlists.EventListContainer}.
 */
interface BehaviorDecorator<E> {
	/**
	 * This will be called when the decorator is installed to register the consumer
	 * that should be called when an object is updated (needed by
	 * {@link PropertyHandlerProperty}).
	 */
	default void initUpdateConsumer( Consumer<E> update_dispatcher ) {}


	/**
	 * Class from {@link com.vaadin.data.Container} that this decorator is implementing.
	 */
	Class containerFunctionalityClass();

	/**
	 * Wrap the given source list to add functionality.
	 */
	EventList<E> wrap( EventList<E> source, PropertyHandler<E> property_handler );

	/**
	 * Handle a method from the class returned from {@link #containerFunctionalityClass()}.
	 */
	Object handleMethod( Method method, Object... args ) throws Exception;
}
