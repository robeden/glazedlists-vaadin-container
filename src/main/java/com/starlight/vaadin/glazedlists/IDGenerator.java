package com.starlight.vaadin.glazedlists;

/**
 * Determines the ID for a given object.
 */
public interface IDGenerator<E> {
	IDGenerator IDENTITY = System::identityHashCode;


	Object objectToId( E object );



	static <E> IDGenerator<E> identity() {
		//noinspection unchecked
		return IDENTITY;
	}
}
