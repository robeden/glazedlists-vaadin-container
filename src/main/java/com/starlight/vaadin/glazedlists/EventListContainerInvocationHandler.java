package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.util.concurrent.Lock;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.AbstractContainer;
import com.vaadin.ui.UI;
import com.vaadin.ui.UIDetachedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;


/**
 *
 */
@SuppressWarnings( { "UnnecessaryBoxing", "UnnecessaryUnboxing" } )
class EventListContainerInvocationHandler<E> extends AbstractContainer
	implements Container.ItemSetChangeNotifier, Container.Indexed, EventListContainer<E>,
	InvocationHandler {

	private static final Logger LOG =
		LoggerFactory.getLogger( EventListContainerInvocationHandler.class );



	protected final PropertyHandler<E> property_handler;
	private final UI associated_ui;

	private final List<BehaviorDecorator<E>> decorators;

	private final IDManager<E> id_manager;

	private final ListEventListener<E> source_list_listener;



	// The purpose of this consumer is to ensure an event is dispatched if the object
	// is updated.
	//
	// Implementation note: there are several ways to do this including firing the
	//                      ItemSetChangeEvent directly from here. I'm going with the
	//                      "kick the list" approach at the moment because it's the
	//                      friendliest if the list is used as the source for another
	//                      list. Since we're changing the object, it seems reasonable
	//                      to be responsible for updating the list chain.
	private final Consumer<E> update_dispatcher = ( obj ) -> {
		EventList<E> source_list = this.source_list;
		source_list.getReadWriteLock().writeLock().lock();
		try {
			int index = source_list.indexOf( obj );
			source_list.set( index, source_list.get( index ) );
		}
		finally {
			source_list.getReadWriteLock().writeLock().unlock();
		}
	};


	private volatile EventList<E> source_list;
	private volatile Collection<EventList<E>> lists_to_dispose;
	private volatile EventList<E> user_source_list;
	private volatile String source_list_id_for_logging;

	private volatile boolean disposed = false;


	/**
	 * Create an instance.
	 *
	 * @param associated_ui     The UI associated with the container. This is necessary
	 *                          to ensure the correct session lock is used when
	 *                          dispatching change events.
	 */
	EventListContainerInvocationHandler( EventList<E> source_list,
		PropertyHandler<E> property_handler, IDManager<E> id_manager,
		UI associated_ui, List<BehaviorDecorator<E>> decorators,
		int max_events_per_block ) {

		requireNonNull( source_list );

		this.property_handler = requireNonNull( property_handler );
		this.id_manager = requireNonNull( id_manager );
		this.associated_ui = requireNonNull( associated_ui );

		if ( decorators == null ) this.decorators = Collections.emptyList();
		else {
			this.decorators = decorators;
			decorators.forEach(
				decorator -> decorator.initUpdateConsumer( update_dispatcher ) );
		}


		source_list_listener = new ImmediateDispatchingListener( max_events_per_block );

		setSource( source_list, false );    // no source change event needed
	}



	@Override
	public Object invoke( Object proxy, Method method, Object[] args ) throws Throwable {
		Class declaring_class = method.getDeclaringClass();

		if ( disposed ) {
			// Prevent multiple disposals
			if ( method.getName().equals( "dispose" ) ) return null;

			throw new IllegalStateException( "Container cannot be used after disposal" );
		}

		try {
			// See if it's handled by a decorator
			for ( BehaviorDecorator decorator : decorators ) {
				if ( declaring_class.equals( decorator.containerFunctionalityClass() ) ) {

					return decorator.handleMethod( method, args );
				}
			}

			// Otherwise handle in this class
			return method.invoke( this, args );
		}
		catch( InvocationTargetException ex ) {
			throw ex.getCause();
		}
	}


	///////////////////////////////////////////////
	// EventListContainer methods


	@Override
	public EventList<E> getSource() {
		return user_source_list;
	}

	@Override
	public EventList<E> getTransformedSource() {
		return source_list;
	}

	/**
	 * Change the source list.
	 */
	@Override
	public void setSource( EventList<E> source_list ) {
		setSource( source_list, true );
	}

	private void setSource( EventList<E> user_source_list, boolean fire_source_change ) {
		if ( user_source_list == null ) {
			throw new IllegalArgumentException( "source_list cannot be null" );
		}

		EventList<E> old_source_list = this.source_list;

		if ( old_source_list != null ) {
			try {
				old_source_list.removeListEventListener( source_list_listener );
			}
			catch( IllegalArgumentException ex ) {
				// ignore, listener
			}
		}
		Collection<EventList<E>> to_dispose = lists_to_dispose;
		if ( to_dispose != null ) to_dispose.forEach( EventList::dispose );


		EventList<E> list = user_source_list;

		to_dispose = null;
		for( BehaviorDecorator<E> decorator : decorators ) {
			list = decorator.wrap( list, property_handler );

			if ( to_dispose == null ) to_dispose = new ArrayList<>();
			to_dispose.add( list );
		}

		this.user_source_list = user_source_list;
		this.lists_to_dispose = to_dispose == null ? Collections.emptySet() : to_dispose;
		this.source_list = list;
		this.source_list_id_for_logging =
			String.valueOf( System.identityHashCode( list ) );
		id_manager.setSource( list );

		list.addListEventListener( source_list_listener );  // NOTE: Fire after IDManager
															//       has received events

		if ( fire_source_change ) {
			fireEvents( null, null );
		}
	}


	/**
	 * Free all resources (e.g., listeners) associated with the container. This should
	 * be called when the container is no longer used to prevent memory leaks.
	 */
	@Override
	public void dispose() {
		final EventList<E> source_list = this.source_list;
		if ( source_list != null ) {
			try {
				source_list.removeListEventListener( source_list_listener );
			}
			catch( IllegalArgumentException ex ) {
				// ignore, listener is already removed
			}
		}

		lists_to_dispose.forEach( EventList::dispose );

		id_manager.dispose();
		disposed = true;
	}

	/**
	 * Returns the object from the source list that matches the given item ID.
	 */
	@Override
	public E getSourceObject( Object item_id ) {
		if ( item_id == null ) return null;

		return id_manager.getSourceObject( item_id );
	}


	/**
	 * Returns the object from the source list that corresponds to the given item.
	 */
	@Override
	public E getSourceObject( Item item ) {
		if ( item == null ) return null;

		//noinspection unchecked
		PropertyHandlerItem<E> ph_item = ( PropertyHandlerItem<E> ) item;
		return ph_item.getObject();
	}


	///////////////////////////////////////////////
	// Container methods

	@Override
	public Item getItem( Object itemId ) {
		E sourceObject = getSourceObject( itemId );
		if ( sourceObject == null ) return null;

		return new PropertyHandlerItem<>( sourceObject, property_handler,
			update_dispatcher );
	}

	@Override
	public Collection<?> getItemIds() {
		final int size = size();
		if ( size == 0 ) return Collections.emptySet();

		return id_manager.getItemIds( 0, size );
	}

	@Override
	public Property getContainerProperty( final Object itemId, Object propertyId ) {
		if ( itemId == null ) return null;

		E object = id_manager.getSourceObject( itemId );
		if ( object == null ) return null;


		return new PropertyHandlerProperty<>( object, propertyId, property_handler,
			update_dispatcher );
	}

	@Override
	public int size() {
		return id_manager.size();
	}

	@Override
	public boolean containsId( Object itemId ) {
		return itemId != null && id_manager.containsId( itemId );

	}

	@Override
	public Item addItem( Object itemId ) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object addItem() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeItem( Object itemId ) throws UnsupportedOperationException {
		if ( itemId == null ) return false;

		final EventList<E> source_list = this.source_list;
		final Lock lock = source_list.getReadWriteLock().writeLock();
		lock.lock();
		try {
			int index = indexOfId( itemId );
			if ( index < 0 || index >= source_list.size() ) return false;

			source_list.remove( index );
			return true;
		}
		finally {
			lock.unlock();
		}
	}

	@Override
	public boolean removeAllItems() throws UnsupportedOperationException {
		final EventList<E> source_list = this.source_list;
		final Lock lock = source_list.getReadWriteLock().writeLock();
		lock.lock();
		try {
			source_list.clear();
			return true;
		}
		finally {
			lock.unlock();
		}
	}


	///////////////////////////////////////////
	// Property-related methods

	@Override
	public Collection<?> getContainerPropertyIds() {
		return property_handler.getPropertyIds();
	}

	@Override
	public Class<?> getType( Object propertyId ) {
		return property_handler.getType( propertyId );
	}

	@Override
	public boolean addContainerProperty( Object propertyId, Class<?> type,
		Object defaultValue ) throws UnsupportedOperationException {

		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeContainerProperty( Object propertyId )
		throws UnsupportedOperationException {

		throw new UnsupportedOperationException();
	}


	//////////////////////////////////////////
	// ItemSetChangeNotifier

	@Override
	public void addItemSetChangeListener( ItemSetChangeListener listener ) {
		super.addItemSetChangeListener( listener );
	}

	@SuppressWarnings( "deprecation" )
	@Override
	public void addListener( ItemSetChangeListener listener ) {
		super.addListener( listener );
	}

	@Override
	public void removeItemSetChangeListener(
		ItemSetChangeListener listener ) {
		super.removeItemSetChangeListener( listener );
	}

	@SuppressWarnings( "deprecation" )
	@Override
	public void removeListener( ItemSetChangeListener listener ) {
		super.removeListener( listener );
	}


	//////////////////////////////////////////
	// Indexed Container methods

	@Override
	public Item addItemAt( int index, Object newItemId )
		throws UnsupportedOperationException {

		throw new UnsupportedOperationException();
	}

	@Override
	public Object addItemAt( int index ) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<?> getItemIds( int startIndex, int numberOfItems ) {
		return id_manager.getItemIds( startIndex, numberOfItems );
	}

	@Override
	public Object getIdByIndex( int index ) {
		return id_manager.getIdByIndex( index );
	}

	@Override
	public int indexOfId( Object itemId ) {
		return id_manager.indexOfId( itemId );
	}


	/////////////////////////////////////////
	// Ordered Container methods

	@Override
	public Object nextItemId( Object itemId ) {
		return id_manager.nextItemId( itemId );
	}

	@Override
	public Object prevItemId( Object itemId ) {
		return id_manager.prevItemId( itemId );
	}

	@Override
	public Object firstItemId() {
		return id_manager.firstItemId();
	}

	@Override
	public Object lastItemId() {
		return id_manager.lastItemId();
	}

	@Override
	public boolean isFirstId( Object itemId ) {
		return id_manager.isFirstId( itemId );
	}

	@Override
	public boolean isLastId( Object itemId ) {
		return id_manager.isLastId( itemId );
	}

	@Override
	public Object addItemAfter( Object previousItemId )
		throws UnsupportedOperationException {

		throw new UnsupportedOperationException();
	}

	@Override
	public Item addItemAfter( Object previousItemId, Object newItemId )
		throws UnsupportedOperationException {

		throw new UnsupportedOperationException();
	}



	private void fireEvents( ItemSetChangeEvent event1, ItemSetChangeEvent event2 ) {
		//noinspection ConstantConditions
		assert event2 == null || event1 != null :
			"If event2 is not null, event1 must be non-null (event1=" + event1 +
			" event2=" + event2 + ")";

		try {
			if ( event1 == null ) fireItemSetChange();
			else {
				fireItemSetChange( event1 );

				if ( event2 != null ) fireItemSetChange( event2 );
			}
		}
		catch( UIDetachedException ex ) {
			dispose();
		}
	}


	private class ImmediateDispatchingListener implements ListEventListener<E> {
		private final int max_events_per_block;


		ImmediateDispatchingListener( int max_events_per_block ) {
			this.max_events_per_block = max_events_per_block;
		}


		@Override
		public void listChanged( final ListEvent<E> event ) {
			LOG.trace( "Received list event from list {}: {}",
				source_list_id_for_logging, event );

//			System.out.println( "Got event: " + event );
			if ( event.isReordering() ) {
				LOG.trace( "Source list {} was reordered, firing full item set change",
					source_list_id_for_logging );

				// NOTE: Changing to method reference causes BootstrapMethodEror
				//noinspection Convert2MethodRef
				associated_ui.access( () -> fireItemSetChange() );
				return;
			}


			final ItemSetChangeEvent[] events;
			final AtomicInteger next_index = new AtomicInteger( 0 );
			Predicate<ItemSetChangeEvent> event_processor;
			if ( max_events_per_block > 0 && max_events_per_block < Integer.MAX_VALUE ) {
				events = new ItemSetChangeEvent[ max_events_per_block ];
				event_processor = change_event -> {
					int index = next_index.getAndIncrement();
					if ( index >= events.length ) return false;

					events[ index ] = change_event;
					return true;
				};
			}
			else {
				events = null;
				event_processor = change_event -> {
					associated_ui.access( () -> fireItemSetChange( change_event ) );
					return true;
				};
			}

			int block_type = -1;
			int block_start = -1;
			int block_end = -1;
			Object leading_old_id = null;
			Object leading_new_id = null;

			boolean overflow = false;

			while ( event.next() ) {
				// If there's prior data
				if ( block_type != -1 ) {
					if ( !canJoinEvent( block_type, block_start, block_end,
						event.getType(), event.getIndex() ) ) {

						boolean success = buildAndProcessEvent( block_type, block_start,
							block_end, leading_old_id, leading_new_id, event_processor );

						if ( !success ) {
							overflow = true;
							break;
						}

						// fall through...
					}
					else {
						block_end++;
						continue;
					}
				}

				block_type = event.getType();
				block_start = event.getIndex();
				block_end = event.getIndex();
				if ( block_type != ListEvent.DELETE ) {
					leading_new_id = id_manager.getIdByIndex( block_start );
				}

				final E old_value = event.getOldValue();
				if ( block_type != ListEvent.INSERT && old_value != ListEvent.UNKNOWN_VALUE ) {
					leading_old_id = id_manager.getIdForOldValue( block_start, old_value );
				}
			}

			if ( !overflow && block_type != -1 ) {
				overflow = ! buildAndProcessEvent( block_type, block_start,
					block_end, leading_old_id, leading_new_id, event_processor );
			}

			if ( overflow ) {
				LOG.trace( "Max event limit was hit for events from source list {}, " +
					"firing full item set change", source_list_id_for_logging );

				// NOTE: Changing to method reference causes BootstrapMethodEror
				//noinspection Convert2MethodRef
				associated_ui.access( () -> fireItemSetChange() );
			}
			else if ( events != null ) {
				if ( LOG.isTraceEnabled() ) {
					final int count = next_index.get();
					LOG.trace( "Firing {} events for changes from source list {}: {}",
						count, source_list_id_for_logging,
						Arrays.stream( events )
							.limit( count )
							.map( Object::toString )
							.collect( Collectors.joining( ", " ) ) );
				}

				associated_ui.access( () -> {
					for( int i = 0; i < next_index.get(); i++ ) {
						fireItemSetChange( events[ i ] );
					}
				} );
			}
		}


		private boolean canJoinEvent( int block_type, int block_start, int block_end,
			int event_type, int event_index ) {

			if ( block_type != event_type ) return false;

			if ( event_index < block_start ) return false;
			if ( event_index > ( block_end + 1 ) ) return false;

			return true;
		}


		/**
		 * @return      False if the max was hit and an event was dropped.
		 */
		private boolean buildAndProcessEvent( int type, int start, int end,
			Object leading_old_id, Object leading_new_id,
			Predicate<ItemSetChangeEvent> event_processor ) {

			switch ( type ) {
				case ListEvent.DELETE:
					return event_processor.test( new RemovedBlockEvent( start,
						leading_old_id, ( end - start ) + 1,
						EventListContainerInvocationHandler.this ) );

				case ListEvent.INSERT:
					return event_processor.test( new AddedBlockEvent( start,
						leading_new_id, ( end - start ) + 1,
						EventListContainerInvocationHandler.this ) );

				case ListEvent.UPDATE:
					// Seems the best way to do this currently is probably a remove
					// then add. They really need an update event!
					boolean success = event_processor.test( new RemovedBlockEvent( start,
						leading_old_id, ( end - start ) + 1,
						EventListContainerInvocationHandler.this ) );
					return success &&
						event_processor.test( new AddedBlockEvent( start,
							leading_new_id, ( end - start ) + 1,
							EventListContainerInvocationHandler.this ) );

				default:
					// Ack, what's this?
					assert false : "Unexpected event type: " + type;
					return false;
			}
		}
	}


	private static class AddedBlockEvent implements ItemAddEvent {
		private final int start;
		private final Object start_id;
		private final int count;
		private final Container container;

		AddedBlockEvent( int start, Object start_id, int count, Container container ) {
			this.start = start;
			this.start_id = start_id;
			this.count = count;
			this.container = container;
		}


		@Override
		public int getFirstIndex() {
			return start;
		}
		@Override
		public Object getFirstItemId() {
			return start_id;
		}

		@Override
		public int getAddedItemsCount() {
			return count;
		}

		@Override
		public Container getContainer() {
			return container;
		}


		@Override
		public String toString() {
			return "+" + start + "-" + ( start + ( count - 1 ) );
		}
	}


	private static class RemovedBlockEvent implements Container.Indexed.ItemRemoveEvent {
		private final int start;
		private final Object start_id;
		private final int count;
		private final Container container;

		RemovedBlockEvent( int start, Object start_id, int count, Container container ) {
			this.start = start;
			this.start_id = start_id;
			this.count = count;
			this.container = container;
		}


		@Override
		public int getFirstIndex() {
			return start;
		}

		@Override
		public Object getFirstItemId() {
			return start_id;
		}

		@Override
		public int getRemovedItemsCount() {
			return count;
		}

		@Override
		public Container getContainer() {
			return container;
		}


		@Override
		public String toString() {
			return "X" + start + "-" + ( start + ( count - 1 ) );
		}
	}
}
