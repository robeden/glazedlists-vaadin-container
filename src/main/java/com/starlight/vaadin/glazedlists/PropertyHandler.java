package com.starlight.vaadin.glazedlists;

import java.util.Collection;


/**
 * PropertyHandlers contain the knowledge about how to convert between an object contained
 * in a source {@link ca.odell.glazedlists.EventList} and properties used by Vaadin.
 *
 * @see com.starlight.vaadin.glazedlists.SortablePropertyHandler
 */
public interface PropertyHandler<E> {
	/**
	 * Returns the property ID's that will be supported by this handler.
	 */
	public Collection<?> getPropertyIds();

	/**
	 * Return the data type of a given property ID.
	 */
	public Class<?> getType( Object property_id );

	/**
	 * Return the value for a given property from a given object.
	 */
	public Object getValue( E object, Object property_id );

	/**
	 * Set the value for a given property on a given object.
	 */
	public void setValue( E object, Object property_id, Object value )
		throws UnsupportedOperationException;

	/**
	 * Return true if the handler only supports reading.
	 */
	public boolean isReadOnly();
}
