package com.starlight.vaadin.glazedlists;

import java.util.Collection;
import java.util.Comparator;


/**
 * Extension of {@link com.starlight.vaadin.glazedlists.PropertyHandler} which must be
 * implemented to support table sorting.
 *
 * @see {@link com.starlight.vaadin.glazedlists.Builder#sortable()}
 */
public interface SortablePropertyHandler<E> extends PropertyHandler<E> {
	/**
	 * @see com.vaadin.data.Container.Sortable#getSortableContainerPropertyIds()
	 */
	public Collection<?> getSortableContainerPropertyIds();

	/**
	 * Return the comparator for a given propertyId. This should be non-null for any
	 * property contained in the result of {@link #getSortableContainerPropertyIds()}.
	 * Comparators should sort in ascending order. Note that the value passed to the
	 * comparator will be the
	 * {@link com.starlight.vaadin.glazedlists.PropertyHandler#getValue(Object, Object) property value}
	 * rather than the source object.
	 */
	public Comparator<?> getPropertyComparator( Object propertyId );
}
