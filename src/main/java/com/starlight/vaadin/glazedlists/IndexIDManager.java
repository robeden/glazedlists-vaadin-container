package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.EventList;


/**
 *
 */
class IndexIDManager<E> extends AbstractIDManager<E> {
	IndexIDManager( EventList<E> source_list ) {
		super( source_list );
	}



	@Override
	public E getSourceObject( Object id ) {
		final int index = indexOfId( id );
		if ( index < 0 ) return null;

		final EventList<E> source_list = this.source_list;
		source_list.getReadWriteLock().readLock().lock();
		try {
			return source_list.get( index );
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}
	}



	@Override
	public Object getIdByIndex( int index ) {
		return Integer.valueOf( index );
	}

	@Override
	public Object getIdForOldValue( int old_index, E old_value ) {
		return getIdByIndex( old_index );
	}



	@Override
	public int indexOfId( Object item_id ) {
		if ( item_id == null ) return -1;

		return ( ( Integer ) item_id ).intValue();
	}

	@Override
	public boolean containsId( Object item_id ) {
		final int index = indexOfId( item_id );
		if ( index < 0 ) return false;

		final int size = size();
		if ( index >= size ) return false;

		return true;
	}
}
