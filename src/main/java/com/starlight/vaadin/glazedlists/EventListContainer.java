package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.EventList;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.ui.UI;


/**
 * Extension to {@link com.vaadin.data.Container} that is backed by an
 * {@link ca.odell.glazedlists.EventList}.
 */
@SuppressWarnings( "WeakerAccess" )
public interface EventListContainer<E>
	extends Container, Container.ItemSetChangeNotifier, Container.Indexed {

	/**
	 * Change the source list.
	 */
	public void setSource( EventList<E> source_list );

	/**
	 * Get the source list specified via {@link #setSource}. Note that this may not be
	 * the list directly backing the container if additional transformations are applied.
	 *
	 * @see #getTransformedSource
	 */
	public EventList<E> getSource();

	/**
	 * Returns the EventList that directly backs the container with any transformations
	 * (sorting, filtering, etc.) applied to the list supplied via {@link #setSource}.
	 */
	EventList<E> getTransformedSource();

	/**
	 * Free all resources (e.g., listeners) associated with the container. This should
	 * be called when the container is no longer used to prevent memory leaks.
	 */
	public void dispose();


	/**
	 * Returns the object from the source list that matches the given item ID.
	 */
	public E getSourceObject( Object item_id );

	/**
	 * Returns the object from the source list that corresponds to the given item.
	 */
	public E getSourceObject( Item item );


	public static <E> Builder<E> create( EventList<E> source_list,
		PropertyHandler<E> property_handler, UI associated_ui ) {

		return new Builder<>( source_list, property_handler, associated_ui );
	}
}
