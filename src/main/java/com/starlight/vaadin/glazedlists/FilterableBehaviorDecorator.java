package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.FilterList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.matchers.CompositeMatcherEditor;
import ca.odell.glazedlists.matchers.Matcher;
import ca.odell.glazedlists.matchers.MatcherEditor;
import com.vaadin.data.Container;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.util.filter.UnsupportedFilterException;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.function.Consumer;


/**
 *
 */
class FilterableBehaviorDecorator<E> implements BehaviorDecorator<E> {
	private static final Method METHOD_ADD_CONTAINER_FILTER;
	private static final Method METHOD_REMOVE_CONTAINER_FILTER;
	private static final Method METHOD_REMOVE_ALL_CONTAINER_FILTERS;
	private static final Method METHOD_GET_CONTAINER_FILTERS;
	static {
		try {
			METHOD_ADD_CONTAINER_FILTER = Container.Filterable.class.getMethod(
				"addContainerFilter", Container.Filter.class );

			METHOD_REMOVE_CONTAINER_FILTER = Container.Filterable.class.getMethod(
				"removeContainerFilter", Container.Filter.class );

			METHOD_REMOVE_ALL_CONTAINER_FILTERS = Container.Filterable.class.getMethod(
				"removeAllContainerFilters" );

			METHOD_GET_CONTAINER_FILTERS = Container.Filterable.class.getMethod(
				"getContainerFilters" );
		}
		catch( NoSuchMethodException ex ) {
			Error er = new NoSuchMethodError( "Unable to find required method from " +
				"Container.Filterable. Perhaps an incompatible version of Vaadin is " +
				"being used?" );
			er.initCause( ex );
			throw er;
		}
	}


	private final EventList<MatcherEditor<E>> matchers = new BasicEventList<>();
	private PropertyHandler<E> property_handler;
	private Consumer<E> update_dispatcher;



	@Override
	public void initUpdateConsumer( Consumer<E> update_dispatcher ) {
		this.update_dispatcher = update_dispatcher;
	}



	@Override
	public Class containerFunctionalityClass() {
		return Container.Filterable.class;
	}

	@Override
	public EventList<E> wrap( EventList<E> source, PropertyHandler<E> property_handler ) {
		this.property_handler = property_handler;

		source.getReadWriteLock().readLock().lock();
		try {
			return new FilterList<>( source, new CompositeMatcherEditor<>( matchers ) );
		}
		finally {
			source.getReadWriteLock().readLock().unlock();
		}
	}

	@Override
	public Object handleMethod( Method method, Object... args ) throws Exception {
		if ( method.equals( METHOD_ADD_CONTAINER_FILTER ) ) {
			addContainerFilter( ( Container.Filter ) args[ 0 ] );
			return null;
		}
		else if ( method.equals( METHOD_REMOVE_CONTAINER_FILTER ) ) {
			removeContainerFilter( ( Container.Filter ) args[ 0 ] );
			return null;
		}
		else if ( method.equals( METHOD_REMOVE_ALL_CONTAINER_FILTERS ) ) {
			removeAllContainerFilters();
			return null;
		}
		else if ( method.equals( METHOD_GET_CONTAINER_FILTERS ) ) {
			return getContainerFilters();
		}
		else {
			assert false : "Unknown method: " + method;
			return null;
		}
	}

	///////////////////////////////////////////////
	// Container.Filterable methods

	@SuppressWarnings( "WeakerAccess" )
	public void addContainerFilter( Filter filter )
		throws UnsupportedFilterException {

		matchers.getReadWriteLock().writeLock().lock();
		try {
			matchers.add( GlazedLists.fixedMatcherEditor(
				new FilterMatcher<>( filter, property_handler, update_dispatcher ) ) );
		}
		finally {
			matchers.getReadWriteLock().writeLock().unlock();
		}
	}

	@SuppressWarnings( "WeakerAccess" )
	public void removeContainerFilter( Filter filter ) {
		matchers.getReadWriteLock().writeLock().lock();
		try {
			Iterator<MatcherEditor<E>> iterator = matchers.iterator();

			while( iterator.hasNext() ) {
				MatcherEditor<E> editor = iterator.next();
				Matcher<E> matcher = editor.getMatcher();

				// Only interested in matchers that contain Filters
				if ( !( matcher instanceof FilterMatcher ) ) continue;

				if ( filter.equals( ( ( FilterMatcher ) matcher ).getFilter() ) ) {
					iterator.remove();
					return;
				}
			}
		}
		finally {
			matchers.getReadWriteLock().writeLock().unlock();
		}
	}

	@SuppressWarnings( "WeakerAccess" )
	public void removeAllContainerFilters() {
		matchers.getReadWriteLock().writeLock().lock();
		try {
			matchers.clear();
		}
		finally {
			matchers.getReadWriteLock().writeLock().unlock();
		}
	}

	public Collection<Filter> getContainerFilters() {
		Collection<Filter> to_return = new LinkedList<>();

		matchers.getReadWriteLock().readLock().lock();
		try {
			for( MatcherEditor<E> editor : matchers ) {
				Matcher<E> matcher = editor.getMatcher();

				// Only interested in matchers that contain Filters
				if ( !( matcher instanceof FilterMatcher ) ) continue;

				to_return.add( ( ( FilterMatcher ) matcher ).getFilter() );
			}
		}
		finally {
			matchers.getReadWriteLock().readLock().unlock();
		}

		return to_return;
	}
}
