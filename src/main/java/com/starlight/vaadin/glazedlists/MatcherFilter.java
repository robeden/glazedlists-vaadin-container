package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.matchers.Matcher;
import com.vaadin.data.Container;
import com.vaadin.data.Item;


/**
*
*/
class MatcherFilter<E> implements Container.Filter {
	private final Matcher<E> matcher;


	MatcherFilter( Matcher<E> matcher ) {
		this.matcher = matcher;
	}


	@Override
	public boolean passesFilter( Object itemId, Item item )
		throws UnsupportedOperationException {

		//noinspection unchecked
		PropertyHandlerItem<E> internal_item = ( PropertyHandlerItem<E> ) item;
		E object = internal_item.getObject();

		return matcher.matches( object );
	}



	@Override
	public boolean appliesToProperty( Object propertyId ) {
		return true;
	}
}
