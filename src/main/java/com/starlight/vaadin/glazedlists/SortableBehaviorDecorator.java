package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.SortedList;
import com.vaadin.data.Container;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;


/**
 *
 */
class SortableBehaviorDecorator<E> implements BehaviorDecorator<E> {
	private static final Method METHOD_SORTABLE_CONTAINER_PROPERTY_IDS;
	private static final Method METHOD_SORT;
	static {
		try {
			METHOD_SORTABLE_CONTAINER_PROPERTY_IDS = Container.Sortable.class.getMethod(
				"getSortableContainerPropertyIds" );

			METHOD_SORT = Container.Sortable.class.getMethod( "sort",
				Object[].class, boolean[].class );
		}
		catch( NoSuchMethodException ex ) {
			Error er = new NoSuchMethodError( "Unable to find required method from " +
				"Container.Sortable. Perhaps an incompatible version of Vaadin is " +
				"being used?" );
			er.initCause( ex );
			throw er;
		}
	}

	private SortablePropertyHandler<E> property_handler;

	private SortedList<E> sorted_list;
	private Comparator<E> active_comparator = null;


	@Override
	public Class containerFunctionalityClass() {
		return Container.Sortable.class;
	}

	@Override
	public EventList<E> wrap( EventList<E> source, PropertyHandler<E> property_handler ) {
		if ( !( property_handler instanceof SortablePropertyHandler ) ) {
			throw new IllegalArgumentException(
				"PropertyHandler must implement SortablePropertyHandler" );
		}

		this.property_handler = ( SortablePropertyHandler<E> ) property_handler;

		source.getReadWriteLock().readLock().lock();
		try {
			sorted_list = new SortedList<>( source, active_comparator );
		}
		finally {
			source.getReadWriteLock().readLock().unlock();
		}
		return sorted_list;
	}



	@Override
	public Object handleMethod( Method method, Object... args ) throws Exception {
		if ( method.equals( METHOD_SORTABLE_CONTAINER_PROPERTY_IDS ) ) {
			return getSortableContainerPropertyIds();
		}
		else if ( method.equals( METHOD_SORT ) ) {
			sort( ( Object[] ) args[ 0 ], ( boolean[] ) args[ 1 ] );
			return null;
		}
		else {
			assert false : "Unknown method: " + method;
			return null;
		}
	}


	///////////////////////////////////////////////
	// Container.Sortable methods

	@SuppressWarnings( "WeakerAccess" )
	public Collection<?> getSortableContainerPropertyIds() {
		return property_handler.getSortableContainerPropertyIds();
	}

	@SuppressWarnings( "WeakerAccess" )
	public void sort( Object[] propertyIds, boolean[] ascending ) {
		List<Comparator<E>> comparators = new ArrayList<>();

		for( int i = 0; i < propertyIds.length; i++ ) {
			Object propertyId = propertyIds[ i ];
			boolean asc = ascending[ i ];

			Comparator<?> comparator = property_handler.getPropertyComparator( propertyId );
			if ( comparator == null ) {
				throw new UnsupportedOperationException( "Sorting the \"" + propertyId +
					"\" property is not supported by the SortablePropertyHandler" );
			}

			if ( !asc ) {
				comparator = GlazedLists.reverseComparator( comparator );
			}

			comparators.add( new PropertyComparator( propertyId, comparator ) );
		}

		if ( comparators.isEmpty() ) {
			active_comparator = null;
		}
		else if ( comparators.size() == 1 ) {
			active_comparator = comparators.get( 0 );
		}
		else {
			active_comparator = GlazedLists.chainComparators( comparators );
		}

		sorted_list.getReadWriteLock().writeLock().lock();
		try {
			sorted_list.setComparator( active_comparator );
		}
		finally {
			sorted_list.getReadWriteLock().writeLock().unlock();
		}
	}


	private class PropertyComparator implements Comparator<E> {
		private final Object propertyId;
		private final Comparator delegate;

		PropertyComparator( Object propertyId, Comparator delegate ) {
			this.propertyId = propertyId;
			this.delegate = delegate;
		}

		@Override
		public int compare( E o1, E o2 ) {
			Object value1 = property_handler.getValue( o1, propertyId );
			Object value2 = property_handler.getValue( o2, propertyId );

			//noinspection unchecked
			return delegate.compare( value1, value2 );
		}
	}
}
