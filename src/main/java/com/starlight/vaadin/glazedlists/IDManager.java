package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.EventList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 *
 */
interface IDManager<E> {
	void setSource( EventList<E> source_list );
	int size();

	Object getIdByIndex( int index );
	int indexOfId( Object item_id );
	boolean containsId( Object item_id );

	Object getIdForOldValue( int old_index, E old_value );

	E getSourceObject( Object id );


	default void dispose() {}



	default Object nextItemId( Object item_id ) {
		if ( isLastId( item_id ) ) return null;

		int index = indexOfId( item_id );
		return getIdByIndex( index + 1 );
	}

	default Object prevItemId( Object item_id ) {
		if ( isFirstId( item_id ) ) return null;

		int index = indexOfId( item_id );
		return getIdByIndex( index - 1 );
	}

	default Object firstItemId() {
		return getIdByIndex( 0 );
	}

	default Object lastItemId() {
		return getIdByIndex( size() - 1 );
	}

	default boolean isFirstId( Object item_id ) {
		return indexOfId( item_id ) == 0;
	}

	default boolean isLastId( Object item_id ) {
		return indexOfId( item_id ) == size() - 1;
	}

	default List<?> getItemIds( final int start_index, final int number_of_items ) {
		if ( number_of_items == 0 ) return Collections.emptyList();

		List<Object> to_return = new ArrayList<>( number_of_items );
		for( int i = 0; i < number_of_items; i++ ) {
			final int index = i + start_index;
			to_return.add( getIdByIndex( index ) );
		}
		return to_return;
	}
}
