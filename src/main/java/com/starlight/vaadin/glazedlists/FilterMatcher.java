package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.matchers.Matcher;
import com.vaadin.data.Container;

import java.util.function.Consumer;


/**
 *
 */
class FilterMatcher<E> implements Matcher<E> {
	private final Container.Filter filter;
	private final PropertyHandler<E> property_handler;
	private final Consumer<E> update_dispatcher;

	FilterMatcher( Container.Filter filter, PropertyHandler<E> property_handler,
		Consumer<E> update_dispatcher ) {

		this.filter = filter;
		this.property_handler = property_handler;
		this.update_dispatcher = update_dispatcher;
	}

	Container.Filter getFilter() {
		return filter;
	}


	@Override
	public boolean matches( E item ) {
		// NOTE: itemId is not supported, but this doesn't appear to matter in any
		//       standard Filters. Supporting it would be difficult and expensive
		return filter.passesFilter( null,
			new PropertyHandlerItem<>( item, property_handler, update_dispatcher ) );
	}
}
