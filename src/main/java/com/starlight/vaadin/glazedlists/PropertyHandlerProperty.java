package com.starlight.vaadin.glazedlists;

import com.vaadin.data.Property;

import java.util.function.Consumer;


/**
 *
 */
class PropertyHandlerProperty<E> implements Property {
	private final E object;
	private final Object propertyId;
	private final PropertyHandler<E> handler;
	private final Consumer<E> value_update_dispatcher;

	private boolean read_only_mode;

	PropertyHandlerProperty( E object, Object propertyId,
		PropertyHandler<E> handler, Consumer<E> value_update_dispatcher ) {

		this.object = object;
		this.propertyId = propertyId;
		this.handler = handler;
		this.value_update_dispatcher = value_update_dispatcher;

		read_only_mode = handler.isReadOnly();
	}

	@Override
	public Object getValue() {
		return handler.getValue( object, propertyId );
	}

	@Override
	public Class getType() {
		return handler.getType( propertyId );
	}

	@Override
	public boolean isReadOnly() {
		return read_only_mode || handler.isReadOnly();
	}

	@Override
	public void setValue( Object newValue ) throws ReadOnlyException {
		if ( read_only_mode ) throw new ReadOnlyException();

		handler.setValue( object, propertyId, newValue );

		value_update_dispatcher.accept( object );
	}

	@Override
	public void setReadOnly( boolean new_status ) {
		if ( !new_status && !handler.isReadOnly() ) {
			throw new UnsupportedOperationException();
		}
		read_only_mode = new_status;
	}
}
