package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.EventList;
import com.vaadin.data.Container;
import com.vaadin.ui.UI;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;


/**
 *
 */
@SuppressWarnings( "WeakerAccess" )
public class Builder<E> {
	private final EventList<E> source_list;
	private final PropertyHandler<E> property_handler;
	private final UI associated_ui;

	private boolean filterable = false;
	private boolean sortable = false;
	private int max_events_per_block = 5;

	private Function<E,Object> id_function = null;
	private boolean cache_function_ids = true;
	private boolean id_by_index = false;
	private boolean id_is_object = false;



	Builder( EventList<E> source_list, PropertyHandler<E> property_handler,
		UI associated_ui ) {

		this.source_list = source_list;
		this.property_handler = property_handler;
		this.associated_ui = associated_ui;
	}



	/**
	 * Make the container {@link com.vaadin.data.Container.Sortable sortable}.
	 * The property handler must implement {@link SortablePropertyHandler}.
	 *
	 * @throws IllegalArgumentException   If the property handler does not
	 *                                    implement {@link SortablePropertyHandler}.
	 */
	public Builder<E> sortable() {
		sortable = true;

		if ( !( property_handler instanceof SortablePropertyHandler ) ) {
			throw new IllegalArgumentException(
				"PropertyHandler must implement SortablePropertyHandler" );
		}

		return this;
	}


	/**
	 * Make the container {@link com.vaadin.data.Container.Filterable filterable}.
	 */
	public Builder<E> filterable() {
		filterable = true;
		return this;
	}


	/**
	 * This determines the maximum number of
	 * {@link com.vaadin.data.Container.ItemSetChangeEvent ItemSetChangeEvents} that will
	 * be dispatched for each {@link ca.odell.glazedlists.event.ListEvent} received from
	 * the source list.
	 */
	public Builder<E> maxEventsPerBlock( int max ) {
		max_events_per_block = max;
		return this;
	}



	/**
	 * Equivalent to {@link #idFunction(Function, boolean) idFunction(function, true)}.
	 */
	public Builder<E> idFunction( Function<E,Object> function ) {
		if ( id_by_index || id_is_object ) {
			throw new IllegalStateException(
				"may not be used in conjunction with idByIndex or idIsObject" );
		}
		id_function = Objects.requireNonNull( function );
		return this;
	}

	/**
	 * A function to convert domain objects to stable container IDs. The default
	 * implementation uses {@link System#identityHashCode(Object)}, but it may be
	 * desirable to use a more precise them utilizing knowledge of the domain object
	 * implementation, in which case this function may be set.
	 * <p></p>
	 * The function must return an ID for every valid object in the source list and each
	 * object returned must uniquely identify the object to which it relates. Also, it
	 * will be called often, so it is critical that it return quickly.
	 * <p></p>
	 * This method is mutually exclusive with {@link #idByIndex()}.
	 *
	 * @param cache_function_ids    If true, a mapping of IDs to objects will be stored.
	 *                              This can increase performance but at the cost of
	 *                              memory.
	 */
	public Builder<E> idFunction( Function<E,Object> function,
		boolean cache_function_ids ) {

		if ( id_by_index || id_is_object ) {
			throw new IllegalStateException(
				"may not be used in conjunction with idByIndex or idIsObject" );
		}
		id_function = Objects.requireNonNull( function );
		this.cache_function_ids = cache_function_ids;
		return this;
	}

	/**
	 * A specialization of {@link #idFunction(Function)} which uses the domain object
	 * as the ID.
	 */
	public Builder<E> idIsObject() {
		if ( id_function != null || id_by_index ) {
			throw new IllegalStateException(
				"may not be used in conjunction with idByIndex or idFunction" );
		}
		id_is_object = true;
		return this;
	}

	/**
	 * This is a specialization of {@link #idFunction(Function)} which uses domain
	 * object's index in the source list as an identifier. Doing so is highly optimal, but
	 * can only work under the following condition: <b>items in the list which are not at
	 * the last index may not be removed.</b> The follow ARE allowed:
	 * <ul>
	 *     <li>Appending items to the list</li>
	 *     <li>Removing the last item from the list</li>
	 *     <li>Clearing the list</li>
	 *     <li>Editing items in the list (i.e., {@link EventList#set(int, Object)})</li>
	 * </ul>
	 * <p></p>
	 * This method is mutually exclusive with {@link #idFunction(Function)}.
	 */
	public Builder<E> idByIndex() {
		if ( id_function != null || id_is_object ) {
			throw new IllegalStateException(
				"may not be used in conjunction with idFunction or idIsObject" );
		}
		id_by_index = true;
		return this;
	}



	/**
	 * Create the {@link EventListContainer}.
	 */
	public EventListContainer<E> build() {
		List<BehaviorDecorator<E>> decorators = new ArrayList<>();

		List<Class> interfaces = new ArrayList<>();

		// Support the interfaces of EventListContainerInvocationHandler
		// (except InvocationHandler)
		for( Class ifc : EventListContainerInvocationHandler.class.getInterfaces() ) {
			if ( ifc.equals( InvocationHandler.class ) ) continue;

			interfaces.add( ifc );
		}

		if ( filterable ) {
			interfaces.add( Container.Filterable.class );
			decorators.add( new FilterableBehaviorDecorator<>() );
		}

		if ( sortable ) {
			interfaces.add( Container.Sortable.class );
			decorators.add( new SortableBehaviorDecorator<>() );
		}

		IDManager<E> id_manager;
		if ( id_by_index ) {
			id_manager = new IndexIDManager<>( source_list );
		}
		else if ( id_is_object ) {
			id_manager = new IdentityIDManager<>( source_list );
		}
		else if ( id_function != null ) {
			id_manager = new MappingIDManager<>( source_list, id_function,
				cache_function_ids );
		}
		else {
			id_manager = new MappingIDManager<>( source_list, System::identityHashCode,
				cache_function_ids );
		}


		InvocationHandler handler = new EventListContainerInvocationHandler<>(
			source_list, property_handler, id_manager, associated_ui, decorators,
			max_events_per_block );

		Object proxy = Proxy.newProxyInstance(
			Builder.class.getClassLoader(),
			interfaces.toArray( new Class[ interfaces.size() ] ), handler );

		//noinspection unchecked
		return ( EventListContainer<E> ) proxy;
	}
}
