package com.starlight.vaadin.glazedlists;

import com.vaadin.data.Item;
import com.vaadin.data.Property;

import java.util.Collection;
import java.util.function.Consumer;


/**
 *
 */
class PropertyHandlerItem<E> implements Item {
	private final E object;
	private final PropertyHandler<E> property_handler;
	private final Consumer<E> update_dispatcher;

	PropertyHandlerItem( E object, PropertyHandler<E> property_handler,
		Consumer<E> update_dispatcher ) {

		this.object = object;
		this.property_handler = property_handler;
		this.update_dispatcher = update_dispatcher;
	}

	@Override
	public Property getItemProperty( Object propertyId ) {
		return new PropertyHandlerProperty<>( object, propertyId, property_handler,
			update_dispatcher );
	}

	@Override
	public Collection<?> getItemPropertyIds() {
		return property_handler.getPropertyIds();
	}

	@Override
	public boolean addItemProperty( Object id, Property property )
		throws UnsupportedOperationException {

		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeItemProperty( Object id ) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}


	E getObject() {
		return object;
	}
}
