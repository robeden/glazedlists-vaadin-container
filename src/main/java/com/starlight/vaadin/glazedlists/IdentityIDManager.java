package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.EventList;


/**
 *
 */
class IdentityIDManager<E> extends AbstractIDManager<E> {
	IdentityIDManager( EventList<E> source_list ) {
		super( source_list );
	}



	@Override
	public E getSourceObject( Object id ) {
		//noinspection unchecked
		return ( E ) id;
	}



	@Override
	public Object getIdByIndex( int index ) {
		source_list.getReadWriteLock().readLock().lock();
		try {
			return source_list.get( index );
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}
	}

	@Override
	public Object getIdForOldValue( int old_index, E old_value ) {
		return old_value;
	}



	@Override
	public int indexOfId( Object item_id ) {
		source_list.getReadWriteLock().readLock().lock();
		try {
			//noinspection SuspiciousMethodCalls
			return source_list.indexOf( item_id );
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}
	}

	@Override
	public boolean containsId( Object item_id ) {
		source_list.getReadWriteLock().readLock().lock();
		try {
			//noinspection SuspiciousMethodCalls
			return source_list.contains( item_id );
		}
		finally {
			source_list.getReadWriteLock().readLock().unlock();
		}
	}
}
