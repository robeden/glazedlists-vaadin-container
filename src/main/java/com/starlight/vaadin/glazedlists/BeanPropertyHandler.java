package com.starlight.vaadin.glazedlists;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;


/**
 * A {@link PropertyHandler} implementation that pulls properties from a bean.
 * For example:
 * <pre>
 *     class Person {
 *         public String getFirstName();
 *         public String getLastName();
 *         public int getAge();
 *     }
 * </pre>
 * ... would have properties "firstName", "lastName" and "age".
 * <p></p>
 * Writing is supported if the bean has at least one writable property
 * (with a set method).
 */
public class BeanPropertyHandler<E> implements PropertyHandler<E> {
	private final Collection<String> property_ids;
	private final Map<String,PropertyDescriptor> descriptor_map;
	private final boolean is_somewhat_writable;


	/**
	 * See {@link Introspector#getBeanInfo(Class)}
	 */
	@SuppressWarnings( "WeakerAccess" )
	public BeanPropertyHandler( Class<? extends E> bean_class )
		throws IntrospectionException {

		this( Introspector.getBeanInfo( bean_class ) );
	}

	/**
	 * See {@link Introspector#getBeanInfo(Class,Class)}
	 */
	@SuppressWarnings( "unused" )
	public BeanPropertyHandler( Class<? extends E> bean_class, Class<?> stop_class )
		throws IntrospectionException {

		this( Introspector.getBeanInfo( bean_class, stop_class ) );
	}

	/**
	 * See {@link Introspector#getBeanInfo(Class,Class,int)}
	 */
	@SuppressWarnings( "unused" )
	public BeanPropertyHandler( Class<? extends E> bean_class, Class<?> stop_class,
		int flags ) throws IntrospectionException {

		this( Introspector.getBeanInfo( bean_class, stop_class, flags ) );
	}

	/**
	 * See {@link Introspector#getBeanInfo(Class,int)}
	 */
	@SuppressWarnings( "unused" )
	public BeanPropertyHandler( Class<? extends E> bean_class, int flags )
		throws IntrospectionException {

		this( Introspector.getBeanInfo( bean_class, flags ) );
	}


	private BeanPropertyHandler( BeanInfo info ) {
		final PropertyDescriptor[] descriptors = info.getPropertyDescriptors();

		Set<String> property_ids = new HashSet<>( descriptors.length );
		Map<String,PropertyDescriptor> descriptor_map = new HashMap<>();
		boolean found_writable = false;
		for( PropertyDescriptor descriptor : descriptors ) {
			if ( descriptor.getReadMethod() == null ) continue;

			property_ids.add( descriptor.getName() );

			descriptor_map.put( descriptor.getName(), descriptor );

			if ( descriptor.getWriteMethod() != null ) found_writable = true;
		}

		this.property_ids = Collections.unmodifiableCollection( property_ids );
		this.descriptor_map = descriptor_map;
		this.is_somewhat_writable = found_writable;
	}



	@Override
	public Collection<?> getPropertyIds() {
		return property_ids;
	}



	@Override
	public Class<?> getType( Object property_id ) {
		//noinspection SuspiciousMethodCalls
		PropertyDescriptor descriptor = descriptor_map.get( property_id );
		if ( descriptor == null ) return null;
		return descriptor.getPropertyType();
	}



	@Override
	public Object getValue( E object, Object property_id ) {
		//noinspection SuspiciousMethodCalls
		PropertyDescriptor descriptor = descriptor_map.get( property_id );
		if ( descriptor == null ) return null;
		try {
			Method method = descriptor.getReadMethod();
			if ( method == null ) throw new UnsupportedOperationException();

			return method.invoke( object );
		}
		catch ( IllegalAccessException | InvocationTargetException e ) {
			throw new RuntimeException( e );
		}
	}



	@Override
	public void setValue( E object, Object property_id, Object value )
		throws UnsupportedOperationException {

		//noinspection SuspiciousMethodCalls
		PropertyDescriptor descriptor = descriptor_map.get( property_id );
		if ( descriptor == null ) throw new UnsupportedOperationException();
		try {
			Method method = descriptor.getWriteMethod();
			if ( method == null ) throw new UnsupportedOperationException();

			method.invoke( object, value );
		}
		catch ( IllegalAccessException | InvocationTargetException e ) {
			throw new RuntimeException( e );
		}
	}



	@Override
	public boolean isReadOnly() {
		return !is_somewhat_writable;
	}
}
