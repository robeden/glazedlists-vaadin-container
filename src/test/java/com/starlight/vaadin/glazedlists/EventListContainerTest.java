package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.DebugList;
import ca.odell.glazedlists.EventList;
import com.vaadin.data.Container;
import com.vaadin.ui.UI;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.ArgumentCaptor;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 *
 */
@RunWith( Parameterized.class )
public class EventListContainerTest {
	@Parameterized.Parameters
	public static Collection<TestInput> data() {
		return Arrays.asList(
			// Default - (cache)
			new TestInput( b -> {},
				( index, value ) -> System.identityHashCode( value ), false ),
			// Explicit - no cache
			new TestInput( b -> b.idFunction( System::identityHashCode ),
				( index, value ) -> System.identityHashCode( value ), false ),
			// Explicit - cache
			new TestInput( b -> b.idFunction( System::identityHashCode, true ),
				( index, value ) -> System.identityHashCode( value ), false ),

			// Index
			new TestInput( Builder::idByIndex,
				( index, value ) -> index, true ),

			// Object
			new TestInput( Builder::idIsObject,
				( index, value ) -> value, false ),

			// Value - no cache
			new TestInput( b -> b.idFunction( Long::new ),
				( index, value ) -> new Long( value ), false ),
			// Value - cache
			new TestInput( b -> b.idFunction( Long::new, true ),
				( index, value ) -> new Long( value ), false ) );
	}



	private EventList<String> source;
	private EventListContainer<String> container;
	private UI mock_ui;

	private BiFunction<Integer,String,Object> id_creator;
	private final boolean id_is_index_based;



	public EventListContainerTest( TestInput input ) {
		this.id_is_index_based = input.index_based;

		source = new DebugList<>();
		source.add( "1" );
		source.add( "2" );
		source.add( "3" );
		source.add( "4" );
		source.add( "5" );

		( ( DebugList ) source ).setLockCheckingEnabled( true );

		mock_ui = mock( UI.class );
		// Run events being done in the session lock
		when( mock_ui.access( any( Runnable.class ) ) )
			.then( invocation -> {
				Runnable runnable = ( Runnable ) invocation.getArguments()[ 0 ];
				runnable.run();
				return null;
			} );

		Builder<String> builder =
			EventListContainer.create( source, new TestPropertyHandler(), mock_ui );
		input.builder_config.accept( builder );

		container = builder.build();

		this.id_creator = input.id_creator;
	}

	@After
	public void tearDown() throws Exception {
		if ( container != null ) {
			container.dispose();
		}
	}


	@Test
	public void testOrderedContainer() {
		assertEquals( 5, container.size() );

		assertEquals( idForIndex( 0 ), container.firstItemId() );
		assertEquals( idForIndex( 4 ), container.lastItemId() );

		assertNull( container.nextItemId( container.lastItemId() ) );

		Object propId = container.firstItemId();        // 0
		Object nextPropId = container.nextItemId( propId );
		assertEquals( idForIndex( 0 ), propId );
		assertNull( container.prevItemId( propId ) );
		assertEquals( idForIndex( 1 ), nextPropId );
		assertTrue( container.isFirstId( propId ) );
		assertFalse( container.isLastId( propId ) );

		propId = nextPropId;                            // 1
		nextPropId = container.nextItemId( propId );
		assertEquals( idForIndex( 1 ), propId );
		assertEquals( idForIndex( 0 ), container.prevItemId( propId ) );
		assertEquals( idForIndex( 2 ), nextPropId );
		assertFalse( container.isFirstId( propId ) );
		assertFalse( container.isLastId( propId ) );

		propId = nextPropId;                            // 2
		nextPropId = container.nextItemId( propId );
		assertEquals( idForIndex( 2 ), propId );
		assertEquals( idForIndex( 1 ), container.prevItemId( propId ) );
		assertEquals( idForIndex( 3 ), nextPropId );
		assertFalse( container.isFirstId( propId ) );
		assertFalse( container.isLastId( propId ) );

		propId = nextPropId;                            // 3
		nextPropId = container.nextItemId( propId );
		assertEquals( idForIndex( 3 ), propId );
		assertEquals( idForIndex( 2 ), container.prevItemId( propId ) );
		assertEquals( idForIndex( 4 ), nextPropId );
		assertFalse( container.isFirstId( propId ) );
		assertFalse( container.isLastId( propId ) );

		propId = nextPropId;                            // 4
		nextPropId = container.nextItemId( propId );
		assertEquals( idForIndex( 4 ), propId );
		assertEquals( idForIndex( 3 ), container.prevItemId( propId ) );
		assertNull( nextPropId );
		assertFalse( container.isFirstId( propId ) );
		assertTrue( container.isLastId( propId ) );
	}


	@Test
	public void testGetSourceObject() {
		assertEquals( "1", container.getSourceObject( idForIndex( 0 ) ) );
		assertEquals( "2", container.getSourceObject( idForIndex( 1 ) ) );
		assertEquals( "3", container.getSourceObject( idForIndex( 2 ) ) );
		assertEquals( "4", container.getSourceObject( idForIndex( 3 ) ) );
		assertEquals( "5", container.getSourceObject( idForIndex( 4 ) ) );

		assertNull( container.getSourceObject( idForIndex( 5 ) ) );
	}

	@Test
	public void testEventFire_addSingleEnd() {
		Container.ItemSetChangeListener mock_listener =
			mock( Container.ItemSetChangeListener.class );

		ArgumentCaptor<Container.ItemSetChangeEvent> args =
			ArgumentCaptor.forClass( Container.ItemSetChangeEvent.class );

		container.addItemSetChangeListener( mock_listener );

		source.getReadWriteLock().writeLock().lock();
		try {
			source.add( "6" );
		}
		finally {
			source.getReadWriteLock().writeLock().unlock();
		}

		verify( mock_listener ).containerItemSetChange( args.capture() );

		Container.ItemSetChangeEvent event = args.getValue();
		assertTrue( event instanceof Container.Indexed.ItemAddEvent );

		Container.Indexed.ItemAddEvent add_event = ( Container.Indexed.ItemAddEvent ) event;
		assertEquals( 5, add_event.getFirstIndex() );
		assertEquals( 1, add_event.getAddedItemsCount() );
		assertEquals( idForIndex( 5 ), add_event.getFirstItemId() );
	}

	@Test
	public void testEventFire_addSingleBeginning() {
		Container.ItemSetChangeListener mock_listener =
			mock( Container.ItemSetChangeListener.class );

		ArgumentCaptor<Container.ItemSetChangeEvent> args =
			ArgumentCaptor.forClass( Container.ItemSetChangeEvent.class );

		container.addItemSetChangeListener( mock_listener );

		source.getReadWriteLock().writeLock().lock();
		try {
			// TODO: should explode with index-based manager
			source.add( 0, "999" );
		}
		finally {
			source.getReadWriteLock().writeLock().unlock();
		}

		verify( mock_listener ).containerItemSetChange( args.capture() );

		Container.ItemSetChangeEvent event = args.getValue();
		assertTrue( event instanceof Container.Indexed.ItemAddEvent );

		Container.Indexed.ItemAddEvent add_event = ( Container.Indexed.ItemAddEvent ) event;
		assertEquals( 0, add_event.getFirstIndex() );
		assertEquals( 1, add_event.getAddedItemsCount() );
		assertEquals( idForIndex( 0 ), add_event.getFirstItemId() );
	}

	@Test
	public void testEventFire_addSingleMiddle() {
		Container.ItemSetChangeListener mock_listener =
			mock( Container.ItemSetChangeListener.class );

		ArgumentCaptor<Container.ItemSetChangeEvent> args =
			ArgumentCaptor.forClass( Container.ItemSetChangeEvent.class );

		container.addItemSetChangeListener( mock_listener );

		source.getReadWriteLock().writeLock().lock();
		try {
			// TODO: should explode with index-based manager
			source.add( 3, "999" );
		}
		finally {
			source.getReadWriteLock().writeLock().unlock();
		}

		verify( mock_listener ).containerItemSetChange( args.capture() );

		Container.ItemSetChangeEvent event = args.getValue();
		assertTrue( event instanceof Container.Indexed.ItemAddEvent );

		Container.Indexed.ItemAddEvent add_event = ( Container.Indexed.ItemAddEvent ) event;
		assertEquals( 3, add_event.getFirstIndex() );
		assertEquals( 1, add_event.getAddedItemsCount() );
		assertEquals( idForIndex( 3 ), add_event.getFirstItemId() );
	}

	@Test
	public void testEventFire_addMultiple() {
		Container.ItemSetChangeListener mock_listener =
			mock( Container.ItemSetChangeListener.class );

		ArgumentCaptor<Container.ItemSetChangeEvent> args =
			ArgumentCaptor.forClass( Container.ItemSetChangeEvent.class );

		container.addItemSetChangeListener( mock_listener );

		source.getReadWriteLock().writeLock().lock();
		try {
			source.addAll( Arrays.asList( "900", "901", "902" ) );
		}
		finally {
			source.getReadWriteLock().writeLock().unlock();
		}

		verify( mock_listener ).containerItemSetChange( args.capture() );

		Container.ItemSetChangeEvent event = args.getValue();
		assertTrue( event instanceof Container.Indexed.ItemAddEvent );

		Container.Indexed.ItemAddEvent remove_event =
			( Container.Indexed.ItemAddEvent ) event;
		assertEquals( 5, remove_event.getFirstIndex() );
		assertEquals( 3, remove_event.getAddedItemsCount() );
		assertEquals( idForIndex( 5 ), remove_event.getFirstItemId() );
	}



	@Test
	public void testEventFire_removeSingleEnd() {
		Container.ItemSetChangeListener mock_listener =
			mock( Container.ItemSetChangeListener.class );

		ArgumentCaptor<Container.ItemSetChangeEvent> args =
			ArgumentCaptor.forClass( Container.ItemSetChangeEvent.class );

		container.addItemSetChangeListener( mock_listener );

		final Object remove_id;

		source.getReadWriteLock().writeLock().lock();
		try {
			remove_id = idForIndex( 4 );

			source.remove( 4 );
		}
		finally {
			source.getReadWriteLock().writeLock().unlock();
		}

		verify( mock_listener ).containerItemSetChange( args.capture() );

		Container.ItemSetChangeEvent event = args.getValue();
		assertTrue( event instanceof Container.Indexed.ItemRemoveEvent );

		Container.Indexed.ItemRemoveEvent remove_event =
			( Container.Indexed.ItemRemoveEvent ) event;
		assertEquals( 4, remove_event.getFirstIndex() );
		assertEquals( 1, remove_event.getRemovedItemsCount() );
		assertEquals( remove_id, remove_event.getFirstItemId() );
	}

	@Test
	public void testEventFire_removeSingleBeginning() {
		Container.ItemSetChangeListener mock_listener =
			mock( Container.ItemSetChangeListener.class );

		ArgumentCaptor<Container.ItemSetChangeEvent> args =
			ArgumentCaptor.forClass( Container.ItemSetChangeEvent.class );

		container.addItemSetChangeListener( mock_listener );

		final Object remove_id;

		source.getReadWriteLock().writeLock().lock();
		try {
			remove_id = idForIndex( 0 );

			// TODO: should explode with index-based manager
			source.remove( 0 );
		}
		finally {
			source.getReadWriteLock().writeLock().unlock();
		}

		verify( mock_listener ).containerItemSetChange( args.capture() );

		Container.ItemSetChangeEvent event = args.getValue();
		assertTrue( event instanceof Container.Indexed.ItemRemoveEvent );

		Container.Indexed.ItemRemoveEvent remove_event =
			( Container.Indexed.ItemRemoveEvent ) event;
		assertEquals( 0, remove_event.getFirstIndex() );
		assertEquals( 1, remove_event.getRemovedItemsCount() );
		assertEquals( remove_id, remove_event.getFirstItemId() );
	}

	@Test
	public void testEventFire_removeSingleMiddle() {
		Container.ItemSetChangeListener mock_listener =
			mock( Container.ItemSetChangeListener.class );

		ArgumentCaptor<Container.ItemSetChangeEvent> args =
			ArgumentCaptor.forClass( Container.ItemSetChangeEvent.class );

		container.addItemSetChangeListener( mock_listener );

		final Object remove_id;

		source.getReadWriteLock().writeLock().lock();
		try {
			remove_id = idForIndex( 2 );

			// TODO: should explode with index-based manager
			source.remove( 2 );
		}
		finally {
			source.getReadWriteLock().writeLock().unlock();
		}

		verify( mock_listener ).containerItemSetChange( args.capture() );

		Container.ItemSetChangeEvent event = args.getValue();
		assertTrue( event instanceof Container.Indexed.ItemRemoveEvent );

		Container.Indexed.ItemRemoveEvent remove_event =
			( Container.Indexed.ItemRemoveEvent ) event;
		assertEquals( 2, remove_event.getFirstIndex() );
		assertEquals( 1, remove_event.getRemovedItemsCount() );
		assertEquals( remove_id, remove_event.getFirstItemId() );
	}


	@Test
	public void testEventFire_updateSingleMiddle() {
		Container.ItemSetChangeListener mock_listener =
			mock( Container.ItemSetChangeListener.class );

		ArgumentCaptor<Container.ItemSetChangeEvent> args =
			ArgumentCaptor.forClass( Container.ItemSetChangeEvent.class );

		container.addItemSetChangeListener( mock_listener );

		final Object removed_id;

		source.getReadWriteLock().writeLock().lock();
		try {
			// NOTE: ID will change for identity-based ID's
			removed_id = idForIndex( 2 );

			source.set( 2, "999" );
		}
		finally {
			source.getReadWriteLock().writeLock().unlock();
		}

		verify( mock_listener, times( 2 ) ).containerItemSetChange( args.capture() );

		List<Container.ItemSetChangeEvent> events = args.getAllValues();
		assertEquals( "Expected 2 events: " + events, 2, events.size() );

		Container.ItemSetChangeEvent event = events.get( 0 );
		assertTrue( event instanceof Container.Indexed.ItemRemoveEvent );
		Container.Indexed.ItemRemoveEvent remove_event =
			( Container.Indexed.ItemRemoveEvent ) event;
		assertEquals( 2, remove_event.getFirstIndex() );
		assertEquals( 1, remove_event.getRemovedItemsCount() );
		assertEquals( removed_id, remove_event.getFirstItemId() );

		event = events.get( 1 );
		assertTrue( event instanceof Container.Indexed.ItemAddEvent );
		Container.Indexed.ItemAddEvent add_event =
			( Container.Indexed.ItemAddEvent ) event;
		assertEquals( 2, add_event.getFirstIndex() );
		assertEquals( 1, add_event.getAddedItemsCount() );
		assertEquals( idForIndex( 2 ), add_event.getFirstItemId() );
	}


	@Test
	public void testEventFire_clear() {
		Container.ItemSetChangeListener mock_listener =
			mock( Container.ItemSetChangeListener.class );

		ArgumentCaptor<Container.ItemSetChangeEvent> args =
			ArgumentCaptor.forClass( Container.ItemSetChangeEvent.class );

		container.addItemSetChangeListener( mock_listener );

		final List<Object> ids = new ArrayList<>();

		source.getReadWriteLock().writeLock().lock();
		try {
			for( int i = 0; i < source.size(); i++ ) {
				ids.add( idForIndex( id_is_index_based ? 0 : i ) );
			}

			source.clear();
		}
		finally {
			source.getReadWriteLock().writeLock().unlock();
		}

		// WARNING: GL is currently not batching events correctly, so this assumes
		//          individual events.
		verify( mock_listener, times( 1 ) ).containerItemSetChange( args.capture() );

		List<Container.ItemSetChangeEvent> events = args.getAllValues();
		assertEquals( 1, events.size() );
		for( Container.ItemSetChangeEvent event : events ) {
			assertTrue( event instanceof Container.Indexed.ItemRemoveEvent );

			Container.Indexed.ItemRemoveEvent remove_event =
				( Container.Indexed.ItemRemoveEvent ) event;
			assertEquals( 0, remove_event.getFirstIndex() );
			assertEquals( 5, remove_event.getRemovedItemsCount() );
			assertEquals( ids.get( 0 ), remove_event.getFirstItemId() );
		}
	}

	@Test
	public void testEventFire_removeRange() {
		Container.ItemSetChangeListener mock_listener =
			mock( Container.ItemSetChangeListener.class );

		ArgumentCaptor<Container.ItemSetChangeEvent> args =
			ArgumentCaptor.forClass( Container.ItemSetChangeEvent.class );

		container.addItemSetChangeListener( mock_listener );

		final List<Object> remove_ids = new ArrayList<>();

		source.getReadWriteLock().writeLock().lock();
		try {
			if ( id_is_index_based ) {
				int first_index = source.indexOf( "1" );
				remove_ids.add( idForIndex( first_index ) );
				remove_ids.add( idForIndex( first_index ) );
				remove_ids.add( idForIndex( first_index ) );
			}
			else {
				remove_ids.add( idForIndex( source.indexOf( "1" ) ) );
				remove_ids.add( idForIndex( source.indexOf( "2" ) ) );
				remove_ids.add( idForIndex( source.indexOf( "3" ) ) );
			}

			// TODO: should explode with index-based manager
			source.removeAll( Arrays.asList( "1", "2", "3" ) );
		}
		finally {
			source.getReadWriteLock().writeLock().unlock();
		}

		verify( mock_listener, times( 1 ) ).containerItemSetChange( args.capture() );

		List<Container.ItemSetChangeEvent> events = args.getAllValues();
		assertEquals( 1, events.size() );
		for( Container.ItemSetChangeEvent event : events ) {
			assertTrue( event instanceof Container.Indexed.ItemRemoveEvent );

			Container.Indexed.ItemRemoveEvent remove_event =
				( Container.Indexed.ItemRemoveEvent ) event;
			assertEquals( 0, remove_event.getFirstIndex() );
			assertEquals( 3, remove_event.getRemovedItemsCount() );
			assertEquals( remove_ids.remove( 0 ), remove_event.getFirstItemId() );
		}
	}


	@Test
	public void testDisposal() {
		container.size();

		container.dispose();

		try {
			container.size();
			fail( "Shouldn't have succeeded" );
		}
		catch( IllegalStateException ex ) {
			// expected
		}
	}


//	public void testForLies() {
//		EventList<String> list = GlazedLists.eventListOf( "A", "A", "B", "B", "C", "C" );
//		list.addListEventListener( e -> {
//			System.out.println( e );
//		});
//
//        list.removeAll(GlazedLists.eventListOf("A", "C"));
//
//
//
//		list = GlazedLists.eventListOf( "A", "B", "C", "D", "E" );
//		list.addListEventListener( e -> {
//			System.out.println( e );
//		});
//
//        list.removeAll(GlazedLists.eventListOf("A", "B", "C"));
//	}



	private Object idForIndex( int index ) {
		source.getReadWriteLock().readLock().lock();
		try {
			if ( index < 0 || index >= source.size() ) return null;

			String value = source.get( index );
			return id_creator.apply( index, value );
		}
		finally {
			source.getReadWriteLock().readLock().unlock();
		}
	}



	private static class TestInput {
		private final Consumer<Builder<String>> builder_config;
		private final BiFunction<Integer,String,Object> id_creator;
		private final boolean index_based;


		public TestInput( Consumer<Builder<String>> builder_config,
			BiFunction<Integer,String,Object> id_creator, boolean index_based ) {

			this.builder_config = builder_config;
			this.id_creator = id_creator;
			this.index_based = index_based;
		}
	}
}
