package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.DebugList;
import ca.odell.glazedlists.EventList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.Assert.*;


/**
 *
 */
@RunWith( Parameterized.class )
public class MappingIDManagerTest {
	@Parameterized.Parameters
	public static Collection<TestInput> data() {
		return Arrays.asList(
			new TestInput( System::identityHashCode,
				( index, value ) -> System.identityHashCode( value ), false, false ),
			new TestInput( System::identityHashCode,
				( index, value ) -> System.identityHashCode( value ), true, false ),
			new TestInput( Integer::new,
				( index, value ) -> new Integer( value ), false, true ),
			new TestInput( Integer::new,
				( index, value ) -> new Integer( value ), true, true ) );
	}


	private EventList<String> source;
	private IDManager<String> manager;
	private BiFunction<Integer,String,Object> id_creator;


	public MappingIDManagerTest( TestInput input ) {
		DebugList<String> source = new DebugList<>();
		source.add( "1" );
		source.add( "2" );
		source.add( "3" );
		source.add( "4" );
		source.add( "5" );

		source.setLockCheckingEnabled( true );

		this.source = source;

		manager = new MappingIDManager<>( source, input.function, input.cache_ids );

		this.id_creator = input.id_creator;
	}


	@Test
	public void testSize() throws Exception {
		assertEquals( 5, manager.size() );
	}

	@Test
	public void testGetIdByIndex() {
		assertEquals( idForIndex( 0 ), manager.getIdByIndex( 0 ) );
		assertEquals( idForIndex( 1 ), manager.getIdByIndex( 1 ) );
		assertEquals( idForIndex( 2 ), manager.getIdByIndex( 2 ) );
		assertEquals( idForIndex( 3 ), manager.getIdByIndex( 3 ) );
		assertEquals( idForIndex( 4 ), manager.getIdByIndex( 4 ) );
	}

	@Test
	public void testIndexOfId() {
		assertEquals( 0, manager.indexOfId( idForIndex( 0 ) ) );
		assertEquals( 1, manager.indexOfId( idForIndex( 1 ) ) );
		assertEquals( 2, manager.indexOfId( idForIndex( 2 ) ) );
		assertEquals( 3, manager.indexOfId( idForIndex( 3 ) ) );
		assertEquals( 4, manager.indexOfId( idForIndex( 4 ) ) );

		assertEquals( -1, manager.indexOfId( new Object() ) );
	}

	@Test
	public void testNextItemId() {
		assertEquals( idForIndex( 1 ), manager.nextItemId( idForIndex( 0 ) ) );
		assertEquals( idForIndex( 2 ), manager.nextItemId( idForIndex( 1 ) ) );
		assertEquals( idForIndex( 3 ), manager.nextItemId( idForIndex( 2 ) ) );
		assertEquals( idForIndex( 4 ), manager.nextItemId( idForIndex( 3 ) ) );
		assertNull( manager.nextItemId( idForIndex( 4 ) ) );
	}

	@Test
	public void testPrevItemId() {
		assertEquals( idForIndex( 0 ), manager.prevItemId( idForIndex( 1 ) ) );
		assertEquals( idForIndex( 1 ), manager.prevItemId( idForIndex( 2 ) ) );
		assertEquals( idForIndex( 2 ), manager.prevItemId( idForIndex( 3 ) ) );
		assertEquals( idForIndex( 3 ), manager.prevItemId( idForIndex( 4 ) ) );
		assertNull( manager.prevItemId( idForIndex( 0 ) ) );
	}

	@Test
	public void testFirstItemId() {
		assertEquals( idForIndex( 0 ), manager.firstItemId() );
	}

	@Test
	public void testLastItemId() {
		assertEquals( idForIndex( 4 ), manager.lastItemId() );
	}

	@Test
	public void testIsFirstId() {
		assertTrue( manager.isFirstId( idForIndex( 0 ) ) );

		assertFalse( manager.isFirstId( idForIndex( 1 ) ) );
		assertFalse( manager.isFirstId( idForIndex( 2 ) ) );
		assertFalse( manager.isFirstId( idForIndex( 3 ) ) );
		assertFalse( manager.isFirstId( idForIndex( 4 ) ) );
	}

	@Test
	public void testIsLastId() {
		assertTrue( manager.isLastId( idForIndex( 4 ) ) );

		assertFalse( manager.isLastId( idForIndex( 3 ) ) );
		assertFalse( manager.isLastId( idForIndex( 2 ) ) );
		assertFalse( manager.isLastId( idForIndex( 1 ) ) );
		assertFalse( manager.isLastId( idForIndex( 0 ) ) );
	}

	@Test
	public void testGetItemIds() {
		assertEquals( Arrays.asList( idForIndex( 0 ), idForIndex( 1 ) ),
			manager.getItemIds( 0, 2 ) );

		assertEquals( Arrays.asList( idForIndex( 1 ), idForIndex( 2 ) ),
			manager.getItemIds( 1, 2 ) );

		assertEquals(
			Arrays.asList( idForIndex( 1 ), idForIndex( 2 ),
				idForIndex( 3 ), idForIndex( 4 ) ),
			manager.getItemIds( 1, 4 ) );
	}


	@Test
	public void testSetSource() {
		assertEquals( 5, manager.size() );

		DebugList<String> new_source = new DebugList<>();
		new_source.add( "2" );
		new_source.add( "3" );
		new_source.add( "4" );
		new_source.add( "5" );
		new_source.add( "6" );
		new_source.add( "7" );
		new_source.add( "8" );
		manager.setSource( new_source );

		assertEquals( 7, manager.size() );

		this.source = new_source;
		assertEquals( 0, manager.indexOfId( idForIndex( 0 ) ) );
		assertEquals( 1, manager.indexOfId( idForIndex( 1 ) ) );
		assertEquals( 2, manager.indexOfId( idForIndex( 2 ) ) );
		assertEquals( 3, manager.indexOfId( idForIndex( 3 ) ) );
		assertEquals( 4, manager.indexOfId( idForIndex( 4 ) ) );
		assertEquals( 5, manager.indexOfId( idForIndex( 5 ) ) );
		assertEquals( 6, manager.indexOfId( idForIndex( 6 ) ) );
	}


	private Object idForIndex( int index ) {
		source.getReadWriteLock().readLock().lock();
		try {
			if ( index < 0 || index >= source.size() ) return null;

			String value = source.get( index );
			return id_creator.apply( index, value );
		}
		finally {
			source.getReadWriteLock().readLock().unlock();
		}
	}


	private static class TestInput {
		private final Function<String,Object> function;
		private final BiFunction<Integer,String,Object> id_creator;
		private final boolean cache_ids;
		private final boolean index_based;


		public TestInput( Function<String,Object> function,
			BiFunction<Integer,String,Object> id_creator, boolean cache_ids,
			boolean index_based ) {

			this.function = function;
			this.id_creator = id_creator;
			this.cache_ids = cache_ids;
			this.index_based = index_based;
		}
	}
}