package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.DebugList;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;


/**
 *
 */
public class IndexIDManagerTest {
	private IDManager<String> manager;
	
	@Before
	public void setUp() {
		DebugList<String> source = new DebugList<>();
		source.add( "one" );
		source.add( "two" );
		source.add( "three" );
		source.add( "four" );
		source.add( "five" );

		source.setLockCheckingEnabled( true );

		manager = new IndexIDManager<>( source );
	}



	@Test
	public void testSize() throws Exception {
		assertEquals( 5, manager.size() );
	}

	@Test
	public void testGetIdByIndex() {
		assertEquals( Integer.valueOf( 0 ), manager.getIdByIndex( 0 ) );
		assertEquals( Integer.valueOf( 1 ), manager.getIdByIndex( 1 ) );
		assertEquals( Integer.valueOf( 2 ), manager.getIdByIndex( 2 ) );
		assertEquals( Integer.valueOf( 3 ), manager.getIdByIndex( 3 ) );
		assertEquals( Integer.valueOf( 4 ), manager.getIdByIndex( 4 ) );
	}

	@Test
	public void testIndexOfId() {
		assertEquals( 0, manager.indexOfId( 0 ) );
		assertEquals( 1, manager.indexOfId( 1 ) );
		assertEquals( 2, manager.indexOfId( 2 ) );
		assertEquals( 3, manager.indexOfId( 3 ) );
		assertEquals( 4, manager.indexOfId( 4 ) );
	}

	@Test
	public void testNextItemId() {
		assertEquals( 1, manager.nextItemId( 0 ) );
		assertEquals( null, manager.nextItemId( 4 ) );
	}

	@Test
	public void testPrevItemId() {
		assertEquals( 0, manager.prevItemId( 1 ) );
		assertEquals( null, manager.prevItemId( 0 ) );
	}

	@Test
	public void testFirstItemId() {
		assertEquals( 0, manager.firstItemId() );
	}

	@Test
	public void testLastItemId() {
		assertEquals( 4, manager.lastItemId() );
	}

	@Test
	public void testIsFirstId() {
		assertTrue( manager.isFirstId( 0 ) );
		assertFalse( manager.isFirstId( 1 ) );
	}

	@Test
	public void testIsLastId() {
		assertTrue( manager.isLastId( 4 ) );
		assertFalse( manager.isLastId( 3 ) );
	}

	@Test
	public void testGetItemIds() {
		assertEquals( Arrays.asList( 0, 1 ),
			manager.getItemIds( 0, 2 ) );

		assertEquals( Arrays.asList( 1, 2 ),
			manager.getItemIds( 1, 2 ) );

		assertEquals( Arrays.asList( 1, 2, 3, 4 ),
			manager.getItemIds( 1, 4 ) );
	}


	@Test
	public void testSetSource() {
		assertEquals( 5, manager.size() );

		DebugList<String> new_source = new DebugList<>();
		new_source.add( "a" );
		new_source.add( "b" );
		new_source.add( "c" );
		manager.setSource( new_source );

		assertEquals( 3, manager.size() );
	}
}