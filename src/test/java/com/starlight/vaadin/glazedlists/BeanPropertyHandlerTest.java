package com.starlight.vaadin.glazedlists;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


/**
 *
 */
@RunWith( Parameterized.class )
public class BeanPropertyHandlerTest {
	@Parameterized.Parameters
	public static Collection<TestArgs> params() throws Exception {
		return Arrays.asList(
			new TestArgs( new BeanPropertyHandler<>(
				Person.class, Object.class ), false ),
			new TestArgs( new BeanPropertyHandler<>(
				MutablePerson.class, Object.class ), true ) );
	}



	private final PropertyHandler<Person> handler;
	private final boolean mutable;

	public BeanPropertyHandlerTest( TestArgs args ) {
		this.handler = args.handler;
		this.mutable = args.mutable;
	}


	@Test
	public void propertyIds() {
		Set<String> expected_ids =
			new HashSet<>( Arrays.asList( "firstName", "lastName", "age" ) );
		assertEquals( expected_ids, new HashSet<>( handler.getPropertyIds() ) );
	}

	@Test
	public void type() {
		assertEquals( String.class, handler.getType( "firstName" ) );
		assertEquals( String.class, handler.getType( "lastName" ) );
		assertEquals( Integer.TYPE, handler.getType( "age" ) );
	}

	@Test
	public void getValue() {
		Person george = new Person( "George", "Washington", 284 );
		Person abe = new MutablePerson( "Abraham", "Lincoln", 151 );

		assertEquals( "George", handler.getValue( george, "firstName" ) );
		assertEquals( "Washington", handler.getValue( george, "lastName" ) );
		assertEquals( 284, handler.getValue( george, "age" ) );

		assertEquals( "Abraham", handler.getValue( abe, "firstName" ) );
		assertEquals( "Lincoln", handler.getValue( abe, "lastName" ) );
		assertEquals( 151, handler.getValue( abe, "age" ) );
	}

	@Test
	public void isReadOnly() {
		assertEquals( !mutable, handler.isReadOnly() );
	}

	@Test
	public void setValue_neverWritable() {
		Person george = new Person( "George", "Washington", 284 );

		try {
			handler.setValue( george, "firstName", "Won't Work" );
			fail( "Shouldn't work" );
		}
		catch( IllegalArgumentException ex ) {
			// Expected for mutable version (type doesn't match)
			if ( !mutable ) throw ex;
		}
		catch( UnsupportedOperationException ex ) {
			// Expected for immutable version (type matches, but not supported)
			if ( mutable ) throw ex;
		}
		try {
			handler.setValue( george, "lastName", "Won't Work" );
			fail( "Shouldn't work" );
		}
		catch( IllegalArgumentException ex ) {
			// Expected for mutable version (type doesn't match)
			if ( !mutable ) throw ex;
		}
		catch( UnsupportedOperationException ex ) {
			// Expected for immutable version (type matches, but not supported)
			if ( mutable ) throw ex;
		}
		try {
			handler.setValue( george, "age", 9999 );
			fail( "Shouldn't work" );
		}
		catch( IllegalArgumentException ex ) {
			// Expected for mutable version (type doesn't match)
			if ( !mutable ) throw ex;
		}
		catch( UnsupportedOperationException ex ) {
			// Expected for immutable version (type matches, but not supported)
			if ( mutable ) throw ex;
		}
	}

	@Test
	public void setValue_sometimesWritable() {
		Person abe = new MutablePerson( "Abraham", "Lincoln", 151 );

		try {
			handler.setValue( abe, "firstName", "Might Work" );
			if ( !mutable ) fail( "Shouldn't work" );
		}
		catch( UnsupportedOperationException ex ) {
			if ( mutable ) throw ex;
		}
		try {
			handler.setValue( abe, "lastName", "Might Work" );
			if ( !mutable ) fail( "Shouldn't work" );
		}
		catch( UnsupportedOperationException ex ) {
			if ( mutable ) throw ex;
		}
		try {
			handler.setValue( abe, "age", 9999 );
			if ( !mutable ) fail( "Shouldn't work" );
		}
		catch( UnsupportedOperationException ex ) {
			if ( mutable ) throw ex;
		}
	}




	@SuppressWarnings( { "WeakerAccess", "unused" } )
	public static class Person {
		protected String first_name;
		protected String last_name;
		protected int age;

		public Person( String first_name, String last_name, int age ) {
			this.age = age;
			this.first_name = first_name;
			this.last_name = last_name;
		}

		public String getFirstName() {
			return first_name;
		}

		public String getLastName() {
			return last_name;
		}

		public int getAge() {
			return age;
		}
	}

	@SuppressWarnings( { "WeakerAccess", "unused" } )
	public static class MutablePerson extends Person {
		public MutablePerson( String first_name, String last_name, int age ) {
			super( first_name, last_name, age );
		}

		public void setFirstName( String first_name ) {
			this.first_name = first_name;
		}

		public void setLastName( String last_name ) {
			this.last_name = last_name;
		}

		public void setAge( int age ) {
			this.age = age;
		}
	}


	private static class TestArgs {
		final PropertyHandler<Person> handler;
		final boolean mutable;

		TestArgs( PropertyHandler<Person> handler, boolean mutable ) {
			this.handler = handler;
			this.mutable = mutable;
		}
	}
}