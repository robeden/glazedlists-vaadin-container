package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.DebugList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import com.vaadin.data.Container;
import com.vaadin.ui.UI;
import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

import static org.mockito.Mockito.*;


/**
 *
 */
@SuppressWarnings( "UnnecessaryBoxing" )
public class SortableEventListContainerTest extends TestCase {
	public void testSorting() {
		EventList<MyTestClass> root_list = new DebugList<>();
		root_list.add( new MyTestClass( 2, "two" ) );
		root_list.add( new MyTestClass( 1, "one" ) );
		root_list.add( new MyTestClass( 3, "three" ) );

		( ( DebugList ) root_list ).setLockCheckingEnabled( true );

		PropertyHandler<MyTestClass> property_handler =
			new SortablePropertyHandler<MyTestClass>() {
				@Override
				public Collection<?> getPropertyIds() {
					return Arrays.asList( "number", "text" );
				}

				@Override
				public Class<?> getType( Object property_id ) {
					if ( property_id.equals( "number" ) ) {
						return Integer.class;
					}
					else return String.class;
				}

				@Override
				public Object getValue( MyTestClass object,
					Object property_id ) {

					if ( property_id.equals( "number" ) ) {
						return Integer.valueOf( object.getNumber() );
					}
					else return object.getText();
				}

				@Override
				public void setValue( MyTestClass object,
					Object property_id, Object value ) throws UnsupportedOperationException {

					throw new UnsupportedOperationException();
				}

				@Override
				public boolean isReadOnly() {
					return true;
				}

				@Override
				public Collection<?> getSortableContainerPropertyIds() {
					return getPropertyIds();
				}



				@Override
				public Comparator<?> getPropertyComparator(
					Object propertyId ) {

					if ( propertyId.equals( "number" ) ) {
						return GlazedLists.<Integer>comparableComparator();
					}
					else {
						return String.CASE_INSENSITIVE_ORDER;
					}
				}
			};

		UI mock_ui = mock( UI.class );
		doReturn( null ).when( mock_ui ).access( any( Runnable.class ) );


		EventListContainer<MyTestClass> container = EventListContainer
			.create( root_list, property_handler, mock_ui )
			.sortable()
			.build();
		Container.Sortable sortable_container = ( Container.Sortable ) container;

		Collection<?> sortable_property_ids =
			sortable_container.getSortableContainerPropertyIds();
		assertEquals( 2, sortable_property_ids.size() );
		assertTrue( sortable_property_ids.contains( "number" ) );
		assertTrue( sortable_property_ids.contains( "text" ) );


		// Before sorting
		assertEquals( 3, container.size() );
		assertEquals( "two", container.getItem( container.getIdByIndex( 0 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "one", container.getItem( container.getIdByIndex( 1 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "three", container.getItem( container.getIdByIndex( 2 ) ).
			getItemProperty( "text" ).getValue() );

		// Sort ascending by number
		sortable_container.sort( new Object[] { "number" }, new boolean[] { true } );
		assertEquals( 3, container.size() );
		assertEquals( "one", container.getItem( container.getIdByIndex( 0 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "two", container.getItem( container.getIdByIndex( 1 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "three", container.getItem( container.getIdByIndex( 2 ) ).
			getItemProperty( "text" ).getValue() );

		// Sort descending by number
		sortable_container.sort( new Object[] { "number" }, new boolean[] { false } );
		assertEquals( 3, container.size() );
		assertEquals( "three", container.getItem( container.getIdByIndex( 0 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "two", container.getItem( container.getIdByIndex( 1 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "one", container.getItem( container.getIdByIndex( 2 ) ).
			getItemProperty( "text" ).getValue() );

		verify( mock_ui, times( 2 ) ).access( any( Runnable.class ) );
	}


	public void testNonSortablePropertyHandlerValidation() {

	}


	private class MyTestClass {
		private final int number;
		private final String text;


		private MyTestClass( int number, String text ) {
			this.number = number;
			this.text = text;
		}

		public int getNumber() {
			return number;
		}

		public String getText() {
			return text;
		}
	}
}
