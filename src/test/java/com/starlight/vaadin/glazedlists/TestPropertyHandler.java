package com.starlight.vaadin.glazedlists;

import java.util.Arrays;
import java.util.Collection;


/**
 *
 */
public class TestPropertyHandler implements PropertyHandler<String> {
	@Override
	public Collection<?> getPropertyIds() {
		return Arrays.asList( "forward", "reverse", "number" );
	}

	@Override
	public Class<?> getType( Object property_id ) {
		if ( property_id.equals( "number" ) ) return Integer.class;
		else return String.class;
	}

	@Override
	public Object getValue( String string, Object property_id ) {
		switch( ( String ) property_id ) {
			case "forward":
				return string;

			case "reverse":
			{
				StringBuilder buf = new StringBuilder();
				for( int i = string.length() - 1; i >= 0; i-- ) {
					buf.append( string.charAt( i ) );
				}
				return buf.toString();
			}

			case "number":
			{
				switch( string ) {
					case "zero":
						return 0;
					case "one":
						return 1;
					case "two":
						return 2;
					case "three":
						return 3;
					case "four":
						return 4;
					case "five":
						return 5;
					case "six":
						return 6;
					case "seven":
						return 7;
					case "eight":
						return 8;
					case "nine":
						return 9;
					case "ten":
						return 10;
					default:
						throw new AssertionError( "Unknown number string: " + string );
				}
			}

			default:
				throw new AssertionError( "Invalid propertyId: " + property_id );
		}
	}

	@Override
	public void setValue( String object, Object property_id, Object value )
		throws UnsupportedOperationException {

		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isReadOnly() {
		return true;
	}
}
