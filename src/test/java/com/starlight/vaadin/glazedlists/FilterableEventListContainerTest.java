package com.starlight.vaadin.glazedlists;

import ca.odell.glazedlists.DebugList;
import ca.odell.glazedlists.EventList;
import com.vaadin.data.Container;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.ui.UI;
import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.*;


/**
 *
 */
@SuppressWarnings( "UnnecessaryBoxing" )
public class FilterableEventListContainerTest extends TestCase {
	public void testFiltering() {
		EventList<MyTestClass> root_list = new DebugList<>();
		root_list.add( new MyTestClass( 1, "one" ) );
		root_list.add( new MyTestClass( 2, "two" ) );
		root_list.add( new MyTestClass( 3, "three" ) );

		( ( DebugList ) root_list ).setLockCheckingEnabled( true );

		PropertyHandler<MyTestClass> property_handler = new PropertyHandler<MyTestClass>() {
			@Override
			public Collection<?> getPropertyIds() {
				return Arrays.asList( "number", "text" );
			}

			@Override
			public Class<?> getType( Object property_id ) {
				if ( property_id.equals( "number" ) ) {
					return Integer.class;
				}
				else return String.class;
			}

			@Override
			public Object getValue( MyTestClass object,
				Object property_id ) {

				if ( property_id.equals( "number" ) ) {
					return Integer.valueOf( object.getNumber() );
				}
				else return object.getText();
			}

			@Override
			public void setValue( MyTestClass object,
				Object property_id, Object value ) throws UnsupportedOperationException {

				throw new UnsupportedOperationException();
			}

			@Override
			public boolean isReadOnly() {
				return true;
			}
		};

		UI mock_ui = mock( UI.class );
		doReturn( null ).when( mock_ui ).access( any( Runnable.class ) );

		EventListContainer<MyTestClass> container = EventListContainer
			.create( root_list, property_handler, mock_ui )
			.filterable()
			.build();
		Container.Filterable filterable_container = ( Container.Filterable ) container;

		// Before filtering
		assertEquals( 3, container.size() );
		assertEquals( "one", container.getItem( container.getIdByIndex( 0 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "two", container.getItem( container.getIdByIndex( 1 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "three", container.getItem( container.getIdByIndex( 2 ) ).
			getItemProperty( "text" ).getValue() );



		// Filter numbers less than 2
		Container.Filter less_than_2_filter =
			new Compare.GreaterOrEqual( "number", Integer.valueOf( 2 ) );
		filterable_container.addContainerFilter( less_than_2_filter );
		assertEquals( 2, container.size() );
		assertEquals( "two", container.getItem( container.getIdByIndex( 0 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "three", container.getItem( container.getIdByIndex( 1 ) ).
			getItemProperty( "text" ).getValue() );



		// Filter text that doesn't contain 'e'
		filterable_container.addContainerFilter(
			new SimpleStringFilter( "text", "e", true, false ) );
		assertEquals( 1, container.size() );
		assertEquals( "three", container.getItem( container.getIdByIndex( 0 ) ).
			getItemProperty( "text" ).getValue() );



		// Remove the first filter
		filterable_container.removeContainerFilter( less_than_2_filter );
		assertEquals( 2, container.size() );
		assertEquals( "one", container.getItem( container.getIdByIndex( 0 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "three", container.getItem( container.getIdByIndex( 1 ) ).
			getItemProperty( "text" ).getValue() );



		// Remove all filters
		filterable_container.removeAllContainerFilters();
		assertEquals( 3, container.size() );
		assertEquals( "one", container.getItem( container.getIdByIndex( 0 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "two", container.getItem( container.getIdByIndex( 1 ) ).
			getItemProperty( "text" ).getValue() );
		assertEquals( "three", container.getItem( container.getIdByIndex( 2 ) ).
			getItemProperty( "text" ).getValue() );

		verify( mock_ui, times( 4 ) ).access( any( Runnable.class ) );
	}


	private class MyTestClass {
		private final int number;
		private final String text;


		private MyTestClass( int number, String text ) {
			this.number = number;
			this.text = text;
		}

		int getNumber() {
			return number;
		}

		String getText() {
			return text;
		}
	}
}
