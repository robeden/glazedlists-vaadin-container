This is a Container implementation for Vaadin that is backed by a [GlazedLists](http://www.glazedlists.com) EventList.

This makes your life great in several ways:

* It's easy to use.
* GlazedLists can do a bunch of really powerful transformations on data lists.
* It supports Server Push (introduced in Vaadin 7.1), so all your data changes
  push to the client without any extra work from you (threading is taken care of).
* Sorting and filtering are fully supported.


Links
-----

* [Vaadin Addon Page](http://vaadin.com/addon/glazedlists-vaadin-container)
* [Javadocs](http://robeden.bitbucket.org/javadocs/glazedlists-vaadin-container/)


Example usage
-------------

In this example we will create a data container with properties for a collection of Person
objects. First, we define the Person class:
```
#!java
public class Person {
	public String firstName;
	public String lastName;
	public int age;

	public Person( String firstName, String lastName, int age ) {...}

	// Getters here (exercise for the reader)
}
```

Next use an EventList to store your data objects:
```
#!java
EventList<String> data = new BasicEventList<>();
data.add( new Person(...) );
...
```

Now define a PropertyHandler to define how Vaadin properties are pulled from the data
objects (and anonymous inner class is used here, but not required):
```
#!java
PropertyHandler<Person> handler = new PropertyHandler<Person>() {
	public Collection<?> getPropertyIds() {
		return Arrays.asList( "firstName", "lastName", "age" );
	}

	public Class<?> getType( Object propertyId ) {
		switch( ( String ) propertyId ) {
			case "firstName":
			case "lastName":
				return String.class;
			case "age":
				return Integer.class;

			default:
				throw new IllegalArgumentException();
		}
	}

	public Object getValue( Person person, Object propertyId ) {
		switch( ( String ) propertyId ) {
			case "firstName":
				return person.getFirstName();
			case "lastName":
				return person.getLastName();
			case "age":
				return Integer.valueOf( person.getAge() );

			default:
				throw new IllegalArgumentException();
		}
	}

	public void setValue( Person object, Object propertyId, Object value )
		throws UnsupportedOperationException {

		// Not supporting writing for the moment
		throw new UnsupportedOperationException();
	}

	public boolean isReadOnly() {
		return true;
	}
}
```

Finally, create the container:
```
#!java
// If sorting and/or filtering is desire, that can also be specified here.
// Note that sorting requires implementing SortablePropertyHandler.
EventListContainer<Person> container = EventListContainerFactory.create( data,
	handler, UI.getCurrent() );

// Use the container wherever you normally would...
Table table = new Table( "People", container );
```

As the data list is updated, the table will automatically reflect changes (when server
push is enabled).